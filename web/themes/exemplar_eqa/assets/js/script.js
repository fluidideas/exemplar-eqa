// Custom JS script
// ('body').addClass('page_loading')
function animateScrollTop(target, duration) {
    duration = duration || 16;
    var scrollTopProxy = { value: $(window).scrollTop() };
    if (scrollTopProxy.value != target) {
        $(scrollTopProxy).animate(
            { value: target },
            { duration: duration, step: function (stepValue) {
                var rounded = Math.round(stepValue);
                $(window).scrollTop(rounded);
            }
        });
    }
}

window.onpageshow = function(event) {
    if (event.persisted) {
        window.location.reload()
    }
};

function titleCase(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
        str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    return str.join(' ');
}


function page__loader(event){
	try {
		if ( event.ctrlKey ||  event.metaKey ) {
			//is ctrl + click or cmd + click
		} else {
			$('body').addClass('page__loading');
		}
	} catch (error) {
		$('body').removeClass('page__loading');
	}
}

document.onreadystatechange = () => {
if (document.readyState === 'complete') {
    // document ready
}
};




function createSegmentButtons(){

    var fieldset= $('fieldset[class^="exemplar_webform_page"]'),
        segmentContainer = $('.segment_wrapper'),
        pagesCount = fieldset.length,
        pageTitle = "",
        cnt = 1;



    // Clear the segment_wrapper container;
    segmentContainer.html('');

    // for each fieldset, perform some checks and react accordingly;
    fieldset.each(function () {

        var obj = $(this);
            pageTitle = obj.find('span.fieldset-legend').contents().get(0).nodeValue,
            setText = ( pageTitle.indexOf('-') > -1 ) ? ( pageTitle.substr(0, pageTitle.indexOf(' -') ) ) : ( pageTitle),
            setText = titleCase(setText);

        // If there is more than one page that show the segment_wrapper
        if (pagesCount > 1 ){

            if ($(window).width() <= 767 ) {
                if (obj.find('fieldset.form-item').hasClass('eqa--required') || obj.find('div.form-item').hasClass('eqa--required')){
                    segmentContainer.append('<div class="segment_indicator required" data-targetlink="'+ cnt +'" data-segname="'+setText+'" style="width: calc(100% / 6 - 5px)"></div>');
                } else {
                    segmentContainer.append('<div class="segment_indicator" data-targetlink="'+cnt+'" data-segname="'+setText+'" style="width: calc(100% / 6 - 5px)"></div>');
                }
            } else {
                if (obj.find('fieldset.form-item').hasClass('eqa--required') || obj.find('div.form-item').hasClass('eqa--required')){
                    segmentContainer.append('<div class="segment_indicator required" data-targetlink="'+ cnt +'" data-segname="'+setText+'" style="width: calc(100% / ' + pagesCount + ' - 10px)"></div>');
                } else {
                    segmentContainer.append('<div class="segment_indicator" data-targetlink="'+cnt+'" data-segname="'+setText+'" style="width: calc(100% / ' + pagesCount + ' - 10px)"></div>');
                }
            }

            if (cnt == 1) {
                segmentContainer.find('.segment_indicator').addClass('active');
            }

            cnt++;

        } else {

            if ($(window).width() <= 767 ) {
                if (obj.find('fieldset.form-item').hasClass('eqa--required') || obj.find('div.form-item').hasClass('eqa--required')){
                    segmentContainer.append('<div class="segment_indicator required" data-targetlink="'+ cnt +'" data-segname="'+setText+'" style="width: calc(100% / 6 - 5px)"></div>');
                } else {
                    segmentContainer.append('<div class="segment_indicator" data-targetlink="'+cnt+'" data-segname="'+setText+'" style="width: calc(100% / 6 - 5px)"></div>');
                }
            } else {
                if (obj.find('fieldset.form-item').hasClass('eqa--required') || obj.find('div.form-item').hasClass('eqa--required')){
                    segmentContainer.append('<div class="segment_indicator required" data-targetlink="'+ cnt +'" data-segname="'+setText+'" style="width: calc(100% / 12 - 10px)"></div>');
                } else {
                    segmentContainer.append('<div class="segment_indicator" data-targetlink="'+cnt+'" data-segname="'+setText+'" style="width: calc(100% / 12 - 10px)"></div>');
                }
            }

            if (cnt == 1) {
                segmentContainer.find('.segment_indicator').addClass('active');
            }

            // segmentContainer.hide();

        }

    });

    if (fieldset.length == 1){
        // debugger;
        $('.eqa--next-page').css({"visibility": "hidden", "pointer-events" : "none"});
        $('.webform-button--next, .webform-button--submit').css({"visibility": "visible", "pointer-events" : "visible"});
    }

}


// Run function prior to document.load
createSegmentButtons();


function setSection(target){
    target.preventDefault;
    var next_link = parseInt(target) + 1,
        prev_link = parseInt(target) - 1;
    setupSections(target, next_link, prev_link);
}


function setupSections(target, next_link, prev_link){

    var fieldset  = $('fieldset[class^="exemplar_webform_page"]'),
        pageCount = fieldset.length,
        minPages  = pageCount - 1,
        maxPages  = pageCount + 1;

    $('.segment_indicator').removeClass('active');
    $('fieldset[class^="exemplar_webform_page_"]').fadeOut();
    $('.segment_indicator[data-targetlink=' + target + ']').addClass('active');
    $('.exemplar_webform_page_' + target + '').fadeIn();

    // console.log('prev_link: ' + prev_link + '    minPages: ' + minPages );
	// console.log('next_link: ' + next_link + '    minPages: ' + maxPages );

    if (prev_link <= minPages &&  prev_link > 0){
		// console.log("1");
        $('.eqa--prev-page').attr('onclick', 'setSection('+prev_link+')').html('Move to Section '+prev_link+'').css({"visibility": "visible", "pointer-events" : "visible"});
    } else {
		// console.log("2");
        $('.eqa--prev-page').css({"visibility": "hidden", "pointer-events" : "none"});
    }

    if ( next_link < maxPages ) {
		// console.log("3");
        $('.eqa--next-page').attr('onclick', 'setSection('+next_link+')').html('Move to Section '+next_link+'').css({"visibility": "visible", "pointer-events" : "visible"});
        $('.webform-button--next, .webform-button--submit').css({"visibility": "hidden", "pointer-events" : "none"});
    } else{
		// console.log("4");
        $('.eqa--next-page').css({"visibility": "hidden", "pointer-events" : "none"});
        $('.webform-button--next, .webform-button--submit').css({"visibility": "visible", "pointer-events" : "visible"});
    }

    animateScrollTop($('.exemplar_webform_page_' + target + ''), 1000);
}


function checkboxDisable() {
    // NA checkbox add disabled class to fieldgroups
    var na_checkbox = $('input.checkbox_na');
    na_checkbox.change(function() {

        var fieldgroup = $(this).closest('.fieldgroup');

        if (this.checked) {


            fieldgroup.addClass('--disabled').removeClass('--cb_yes');

            fieldgroup.find('input[type="radio"]').each(function(){
                $(this).prop('checked', false);
            });

            fieldgroup.find('select').each(function(){
                $(this).prop('selectedIndex', 0);
            });

            fieldgroup.find('.js-form-type-textarea textarea').each(function(){
                if (!$.trim($(this).val())) {
                    $(this).closest('.fieldgroup').removeClass('--textarea__has_content').addClass('--textarea__empty');
                    // textarea is empty or contains only white-space
                } else {
                    $(this).closest('.fieldgroup').removeClass('--textarea__empty').addClass('--textarea__has_content');
                }
            });

        } else {

            fieldgroup.removeClass('--disabled').removeClass('--cb_yes');

            fieldgroup.find('.js-form-type-textarea textarea').each(function(){
                if (!$.trim($(this).val())) {
                    $(this).closest('.fieldgroup').removeClass('--textarea__has_content').addClass('--textarea__empty');
                    // textarea is empty or contains only white-space
                } else {
                    $(this).closest('.fieldgroup').removeClass('--textarea__empty').addClass('--textarea__has_content');
                }
            });
        }
    });
}


function connectionCheck() {

    var ajaxReq = new XMLHttpRequest(),
        randomN = Math.round(Math.random() * 100000),
        chkfile = window.location.origin +'/themes/exemplar_eqa/assets/images/live_check.gif';

    ajaxReq.open('HEAD', chkfile + '?rand=' + randomN, false);

    try {
        ajaxReq.send(null);
        if (ajaxReq.status >= 200 && ajaxReq.status < 304 ) {
            return true;
        } else {
            return false;
        }
    } catch(e) {
        return false;
    }
}

function checkSubmit(){
    if (connectionCheck() == true) {
        return true;
    } else {
        return false;
    }
}


$(document).ready(function(){

    var fieldset= $('fieldset[class^="exemplar_webform_page"]'),
        pageCount = fieldset.length;

        $('*[class^="exemplar_webform_page_"]').hide();
        $('.exemplar_webform_page_1').show();

    var getFormType  = $('form').find('[data-webform-key');
    var confirmPage = getFormType.attr('data-webform-key');

    if ( ($('body').hasClass('section-webform') )  && ($('body').is('[class$="-confirmation"]'))     ) {
        $('body').addClass('thankyoupage');
    }

    if (confirmPage == "confirmation") {
        $('body').addClass('confirmation__page');
        $('.segment_indicator').each(function(){
            $(this).removeClass('active');
            var target = $(this).attr('data-targetlink');
            if ( parseInt(target) == pageCount ){
                $(this).addClass('active');
            }
        });
    }

    checkboxDisable();


    $('body.section-viewsubmmission .js-form-type-textarea textarea').keypress(function(event){
        var data = $(this).data();
        var lock = true
        $.each(data, function(key, value) {
            if(key == 'drupalSelector' && value == 'edit-review-review-asana-action'){
                lock = false;
            }
        })
        if(lock){
            event.preventDefault();
        }
    });

    function countChar(val) {
        var len = val.value.length;
        if (len >= 500) {
          val.value = val.value.substring(0, 500);
        } else {
          $('#charNum').text(500 - len);
        }
      };


    // $('body.section-viewsubmmission .js-form-type-textarea textarea').each(function(){
    //     let as = $(this).val().length;
    //     $(this).attr("rows", (Math.round(as/60)));
    // })


    $('.segment_indicator').on("click", function(){
        var target = $(this).attr('data-targetlink'),
            next_link = parseInt(target) + 1
            prev_link = parseInt(target) - 1;
        setupSections(target, next_link, prev_link);
    });

    $('.js-form-type-textarea textarea').on('change', function(){

        if (!$.trim($(this).val())) {
            $(this).closest('.fieldgroup').removeClass('--textarea__has_content').addClass('--textarea__empty');
            // textarea is empty or contains only white-space
        } else {
            $(this).closest('.fieldgroup').removeClass('--textarea__empty').addClass('--textarea__has_content');
        }

    });

    var checkIsValid = true;
    $("form").submit(function() {

		try {
			if ( checkSubmit() == false ) {
				$('body').removeClass('page__loading');
				alert('You have lost your internet connnection, please reconnect to continue');
				checkIsValid = false;
			} else {
				checkIsValid = true;
			}
			return checkIsValid;
		} catch (error) {
			$('body').removeClass('page__loading');
		}

    });

    $('.take20_not_avaliable').each(function(){
        $(this).closest('.daily_checks--form_to_check').addClass('--disabled');
    });

    $('.form_not_avaliable').each(function(){
        $(this).closest('.daily_checks--form_to_check').addClass('--disabled_form');
    });



    // Open modal
    $('#show-modal').click(function(e){
        e.preventDefault();
        $('html').css('overflow-x: visible');
        $('body').addClass('noscroll');
        $('body').addClass('moveRight');
    });

    // Close modal
    $('.close-btn').click(function(e){
        e.preventDefault();
        $('body').removeClass('noscroll');
        $('body').removeClass('moveRight');
        $('html').css('overflow-x: hidden');
    });



    // // LOADER JS
    // document.onreadystatechange = function() {
    //     if (document.readyState !== "complete") {
    //         $('body').addClass('page__loading');
    //     } else {
    //         $('body').removeClass('page__loading');
    //     }
    // };

    window.addEventListener("unload", function(event) {
        //$('body').addClass('page__loading');
        page__loader(event)
    });

    window.addEventListener("beforeunload", function (event) {
        page__loader(event)
   });


    // When clicking the home icon
    // $('#home').on('click', function(event){
	// 	page__loader(event);
    // });

    // // When clicking the save draft icon
    // $('.webform-button--draft').on('click', function(event){
	// 	page__loader(event);
    // });

    // // Home page form buttons
    // $('.section-node-1 .daily_checks--form_to_check').each(function(){

    //     $(this).find('.eqa_button').on('click', function(event){
	// 		page__loader(event);
    //     });
    //     $(this).find('.eqa__view_button').on('click', function(event){
    //         page__loader(event);
    //     });
    //     $(this).find('.eqa__complete_button').on('click', function(event){
	// 		page__loader(event);
    //     });
    // });

    // // Login submit - when valid credentials
    // $("#user-login-form").submit(function () {
	// 	page__loader();
    // });

    // // View Submissions - View button
    // $('.audit-submission-row .views-field-nothing .field-content').each(function(){
    //     $(this).find('a').on('click', function(event){
	// 		page__loader(event);
	// 	});
    // });

    // $('.webform-button--next, .webform-button--previous, .webform-button--submit').on('click', function(event){
	// 	page__loader(event);
    // });







    $('.radio-comment-no').find('input.form-radio').each(function(){

        var btnchecked = false;

        $(this).change(function() {

            var inputVal = $(this).filter(":checked").val();
            var fieldgroup = $(this).closest('.radio-comment-yes');

            if ($(this).prop('checked', true)){
                btnchecked = true;
            }

            if ( (btnchecked == true) && inputVal == 'yes') {
                fieldgroup.addClass('--cb_yes');
                fieldgroup.find('.js-form-type-textarea textarea').each(function(){
                    if (!$.trim($(this).val())) {
                        $(this).closest('.fieldgroup').removeClass('--textarea__has_content').addClass('--textarea__empty');
                    } else {
                        $(this).closest('.fieldgroup').removeClass('--textarea__empty').addClass('--textarea__has_content');
                    }
                });
            } else {
                fieldgroup.removeClass('--cb_yes');
                fieldgroup.find('.js-form-type-textarea textarea').each(function(){
                    if (!$.trim($(this).val())) {
                        $(this).closest('.fieldgroup').removeClass('--textarea__has_content').addClass('--textarea__empty');
                    } else {
                        $(this).closest('.fieldgroup').removeClass('--textarea__empty').addClass('--textarea__has_content');
                    }
                });
            }

        });
    });




});

$(document).ready(function(){

    var modal_info_content  = $('.overview-modal').find('.content-wrapper');
    var formInfoContent     = $('#edit-form-info-content').html();
    var modalButton         = $('span#show-modal');

    modalButton.hide();

    if ( formInfoContent && formInfoContent.length > 20 ) {
        modal_info_content.html('');
        modal_info_content.append(formInfoContent);
        modalButton.show();
    }

	sscheck = 1;
	connectionAlertClosed = false;

	setInterval(() => {
		try {

			if ( checkSubmit() == false && connectionAlertClosed == false ) {
				$('body').removeClass('page__loading');
				alert('You have lost your internet connnection, please reconnect to internet before submitting form');
				checkIsValid = false;
				connectionAlertClosed = true;
			} else if (checkSubmit() == false && connectionAlertClosed == true){
				checkIsValid = false;
				connectionAlertClosed = true;
			} else if (checkSubmit() == true && connectionAlertClosed == true){
				alert('You have now reconnected to the internet, you can now submit data when ready');
				checkIsValid = true;
				connectionAlertClosed = false;
			} else {
				checkIsValid = true;
				connectionAlertClosed == false;
			}


			if ( checkSubmit() == false && connectionAlertClosed == false ) {
				console.log('User has been alerted to disconnection');
			} else if (checkSubmit() == false && connectionAlertClosed == true){
				console.log('Internet connection has yet been re-established');
			} else {
				console.log('Internet connection is valid');
			}

			sscheck++;
			return checkIsValid;
		} catch (error) {
			$('body').removeClass('page__loading');
		}
	}, 10000);

});


$(window).on('load', function(){

    $('body').removeClass('page__loading');

    var webformkey = $('#edit-confirmation').attr('data-webform-key');

    if ( webformkey == 'confirmation' )  {
        console.log('confirmation-page');
        $('.eqa--next-page').css({'visibility':'hidden', 'pointer-events':'none'});
        $('.webform-button--next, .webform-button--submit').css({'visibility':'visible', 'pointer-events':'visible'});
    };

});

// function checkRequiredInputs() {
//     var input = $(':input');
//     input.each(function () {
//         var reqInput = $(this);
//         var reqField = reqInput.closest('.js-webform-states-hidden');
//         if (reqField.attr('required') !== "undefined") {
//             if (!reqField.hasClass('eqa--required')) {
//                 if (reqInput.val().length === 0) {
//                     reqInput.addClass('empty');
//                     reqField.addClass('eqa--required');
//                 }
//             }
//         }
//     });
// }

$(document).ready(function(){
	var windowWidth = $(window).width();
    var resizeTimer;
    $(window).on('resize', function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            if ($(window).width() != windowWidth) {
                windowWidth = $(window).width();
                createSegmentButtons();
            }
		}, 250);
    });
    //checkRequiredInputs();
});