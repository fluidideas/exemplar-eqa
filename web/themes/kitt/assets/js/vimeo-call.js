// Use an external Vimeo API jquery plugin to play/pause/mute/unmute and etc
// Here's the link : https://github.com/jrue/Vimeo-jQuery-API

if($('.video-wrapper').length) {

    $(document).ready(function () {

        // initial setup
        // - video autoplay on/off
        // - audio muted/unmuted
        var isVideoAutoplay = true;
        var isAuidoMuted = true;

        if (isVideoAutoplay === true) {
            $('.video-wrapper').attr('data-video-is-playing', true);
            $('.vi-banner-video').vimeo('play');
            $(".video-control.js-video-control").removeClass('paused').addClass('playing');
        } else {
            $('.video-wrapper').attr('data-video-is-playing', false);
            $('.vi-banner-video').vimeo('pause');
            $('.video-control.js-video-control.paused').addClass("video-control-show");
        }

        if (isAuidoMuted === true) {
            $('.video-wrapper').attr('data-audio-volume', 0);
            $(".vi-banner-video").vimeo("setVolume", 0);
            $(".audio-control.js-audio-control").removeClass('unmuted').addClass('muted');
        } else {
            $('.video-wrapper').attr('data-audio-volume', 1);
            $(".vi-banner-video").vimeo("setVolume", 1);
        }
        
    });

    function playVideo() {
        // var videoId = el.data('video');
        // var video = document.getElementById(videoId);
        videoStatus = $(".video-wrapper").attr('data-video-is-playing');

        if (videoStatus == 'true') {
            // Pause the video
            console.log('trigger to false');
            $('.vi-banner-video').vimeo('pause');
            $(".video-control.js-video-control").removeClass('playing').addClass('paused');
            $('.video-wrapper').attr('data-video-is-playing', false);
            $('.video-control.js-video-control.paused').addClass("video-control-show");
        } else if (videoStatus == 'false') {
            // Play the video
            console.log('trigger to true');
            $('.vi-banner-video').vimeo('play');
            $(".video-control.js-video-control").removeClass('paused').addClass('playing');
            $('.video-wrapper').attr('data-video-is-playing', true);
            $('.video-control.js-video-control').css('opacity', '');
            $('.video-control.js-video-control.playing').removeClass("video-control-show");
        }
    }

    function muteAudio() {

        audioStatus = $(".video-wrapper").attr('data-audio-volume');

        if (audioStatus == 0) {
            // Mute the audio
            $('.video-wrapper').attr('data-audio-volume', 1);
            $(".vi-banner-video").vimeo("setVolume", 1);
            $(".audio-control.js-audio-control").removeClass('muted').addClass('unmuted');
        } else if (audioStatus == 1) {
            // Play the audio
            $('.video-wrapper').attr('data-audio-volume', 0);
            $(".vi-banner-video").vimeo("setVolume", 0);
            $(".audio-control.js-audio-control").removeClass('unmuted').addClass('muted');
        }
    }

    $(document).on('click', '.js-video-control', function (e) {
        playVideo($(this));
        e.preventDefault();
    });

    $(document).on('click', '.js-audio-control', function (e) {
        muteAudio($(this));
        e.preventDefault();
    });

}