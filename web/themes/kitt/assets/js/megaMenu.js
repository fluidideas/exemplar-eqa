//
// Mega menu positioning/functionality
//
var headerHeight = $('#header').outerHeight();
var trigger = $('#header li.menu__item--trigger a');
var megaMenu = $('#header .header__primaryNav .menu__mega-menu');
var overlay = $('.overlay');
var zIndexCount = 0;
var triggerCount = 1;

var includedPaths = [
    '/our-care',
];

// Desktop
if ($(window).width() >= 768) {
    $.each(megaMenu, function (index, value) {
        $('.header__primaryNav .mega-menu__item:nth-child(' + triggerCount + ') a').attr('data-section', $(value).attr('id'));
        triggerCount++;
    });

    megaMenu.css('top', headerHeight);

    trigger.click(function (e) {
        if (includedPaths.indexOf($(this).attr('href')) > -1) {
            e.preventDefault();
            var data = $(this).attr('data-section');
            var selectedTrigger = $(this);

            if (selectedTrigger.hasClass('active')) {
                removeOverlay();
                selectedTrigger.removeClass('active');
                megaMenu.data('section', data).fadeOut(150);
            } else {
                trigger.removeClass('active');
                selectedTrigger.addClass('active');
                megaMenu.each(function () {
                    if ($(this).attr('id') === data) {
                        overlay.show().addClass('active');
                        $(this).css('z-index', zIndexCount);
                        $(this).delay().fadeIn(150);
                        megaMenu.not($(this)).hide();
                    }
                });
                zIndexCount++;
            }
        }
    });

    overlay.click(function () {
        megaMenu.fadeOut(150);
        trigger.removeClass('active');
        removeOverlay();
    });
}