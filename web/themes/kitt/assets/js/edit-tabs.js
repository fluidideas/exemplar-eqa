var $ = jQuery;
var body = $('body');

jQuery(document).ready(function () {

    $('.edit-tabs .tabs li a').each(function (index) {
        var whatIsTheLink = $(this).text();
        $(this).addClass(whatIsTheLink);
    });

    // edit tab
    $('.movepod, .close').click(function () {
        $('.movement-block').fadeToggle();
    });

    var movementDirection;

    $('.movement-arrow').click(function () {
        movementDirection = $(this).attr('direction');
        Cookies.set('admin-tab', movementDirection, {expires: 7});
        $('.edit-tabs').removeClass('hide up down left right').addClass(movementDirection);
        $('.movement-block').fadeToggle();
        if(movementDirection == 'hide'){
            $('body, #toolbar-administration').addClass('hidden-admin');
        }
    });

    $('.hide-button').click(function () {
        $('.edit-tabs').removeClass('hide');
        Cookies.set('admin-tab', 'left', {expires: 7});
        $('body, #toolbar-administration').removeClass('hidden-admin');
    });

    var currentDirection = Cookies.get('admin-tab');

    $('.edit-tabs').addClass(currentDirection);

    console.log(currentDirection);
    if(currentDirection == "hide"){
        $('body, #toolbar-administration').addClass('hidden-admin');
    } else {
        $('body, #toolbar-administration').removeClass('hidden-admin');
    }
  
    // clear cookies
    $('.edit-tabs .tabs ').append('<li><a class="clear">Clear all cookies</a></li>');

    $('.tabs .clear').click(function () {
        Cookies.remove('helper-tool');
        Cookies.remove('admin-tab');
        Cookies.remove('local-styles')
        location.reload();
    });
});