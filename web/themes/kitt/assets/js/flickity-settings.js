var $ = jQuery;

$(document).ready(function () {

    // Default
    var $carousel = $('.carousel');

    $carousel.flickity({
        cellAlign: 'left',
        initialIndex: 0,
        contain: true,
    });

});