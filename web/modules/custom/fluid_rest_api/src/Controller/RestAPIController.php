<?php

namespace Drupal\fluid_rest_api\Controller;

// use GuzzleHttp\Client;
// use Drupal\views\Views;
use Drupal\Core\Controller\ControllerBase;
use Drupal\webform\Entity\Webform;
use Symfony\Component\Yaml\Yaml;
// use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RestAPIController.
 */
class RestAPIController extends ControllerBase
{

    public function __construct() {

        ini_set('memory_limit', '-1');

        $AUTH_USER = 'exemplar-api';
        $AUTH_PASS = 'Rs58DVP5yKgD';
        
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }
        
      }

      public function content() {
         
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        // $database = \Drupal::database();
        // $query = $database->select('webform', 'w')
        // ->fields('w.*');
        // $webformResult = $query->execute();

        // $submission_data = [];

        // $x = 0;

        // foreach ($webformResult as $webformLoop) {

        //     $webform = Webform::load($webformLoop->webform_id);

        //     if($webform){
        //         if ($webform->hasSubmissions()) {

        //             $elements = $webform->getElementsDecoded();
        //             $query = \Drupal::entityQuery('webform_submission')
        //                 ->condition('webform_id', $webformLoop->webform_id)->accessCheck(FALSE);
        //             $result = $query->execute();
                    
        //             $submission_data['formdata'][$webformLoop->webform_id]['form_id'] = $webform->get('id');
        //             $submission_data['formdata'][$webformLoop->webform_id]['form_name'] = $webform->get('title');
        //             $submission_data['formdata'][$webformLoop->webform_id]['form_categories'] = $webform->get('categories');
                    
        //             $query = $database->select('webform_submission_data', 'w')
        //             ->fields('w.*')
        //             ->condition('sid', array_keys($result), 'IN');
        //             $submissions = $query->execute();

        //             foreach($submissions as $submission){

        //                 foreach($elements as $elementList){

        //                     foreach($elementList as $key => $headings){
        //                         if(is_array($headings)){
                                   
        //                             unset($elementList['#wrapper_attributes']);
        //                             $title = $headings['#title'];
        //                             unset($headings['#title']);
        //                             unset($headings['#type']);
                                  
        //                             foreach($headings as $headingKey => $heading){
        //                                 if($submission->name == $headingKey){
        //                                     $submission_data['formdata'][$webformLoop->webform_id]['submissions'][$submission->sid][$key][$headingKey] = $submission;
        //                                     $submission_data['formdata'][$webformLoop->webform_id]['submissions'][$submission->sid][$key][$headingKey]->section = $heading['#section__description'];
        //                                 }
        //                             }
        //                         }
        //                     }
        //                  } 
        //            }
                  
        //         }
               
        //     }
            
            
        //     if($x == 1){
        //         break;
        //     }
        //     $x++;

        // }

        // print_r($submission_data);
        // exit();

        // print_r(json_encode($submission_data));

    // $database = \Drupal::database();
        // $query = $database->select('webform', 'w')
        // ->fields('w.*');
        // $webformResult = $query->execute();


        // $yaml = Yaml::parse(file_get_contents('/path/to/file.yml'));
        // $yamlString = Yaml::dump($yaml);

            //AND changed >= 1597273200 AND changed <= 1597276799 

            //1597317494

        $dateFrom = strtotime(date('Y-m-d ')." - 1 day");
        $dateTo = strtotime(date('Y-m-d')." 24:59:59");
        if(isset($_GET['dateFrom'])){
            $dateFrom = strtotime($_GET['dateFrom']." 00:00:00");
        }

        if(isset($_GET['dateTo'])){
            $dateTo = strtotime($_GET['dateTo']." 24:59:59");
        }


        // ********* GET ALL WEBFORMS *********
        $webformSQL = "select webform_id from webform";	
        $webformResults =  \Drupal::database()->query($webformSQL)->fetchAll();
        
        $formArray = array();
        foreach($webformResults as $webformResult){ 
            array_push($formArray, "'webform.webform.".$webformResult->webform_id."'");
        }

        // ********* END ALL WEBFORMS *********

        // ********* GET ELEMENT DATA *********
        $newConfigArray = $this->getNewConfigArray($formArray);
        
        // ********* END ELEMENT DATA *********
        
        // ********* GET ALL SUBMISSIONS *********
        $querylist = "select * from  webform wf, webform_submission ws, webform_submission_data wsd
            WHERE  ws.webform_id = wf.webform_id
            AND wsd.sid = ws.sid
            AND changed >= ".$dateFrom."
            AND changed <= ".$dateTo."
            AND in_draft = 0
            order by ws.sid
        ";

        $queryResult =  \Drupal::database()->query($querylist)->fetchAll();

        if(empty($queryResult)){
            print "No data";
            exit();
        }

        $webFormDatas = array();
        foreach ($queryResult as $result) {
            $webFormDatas[$result->webform_id][$result->sid][$result->name][] = $result;
        }

        // print "<h1>webFormDatas</h1>";
        // kint($webFormDatas);
        // print "<h1>newConfigArray</h1>";
        // kint($newConfigArray);

        // ********* END ALL SUBMISSIONS *********
        //  print "<pre>";
        $preJsonReturnArray = array();

       // kint($formArray);

        foreach($webFormDatas as $webformID => $webFormData){
         //   kint($webFormData);

            foreach($webFormData as $submissionID => $data){

                $preJsonReturnArray[$webformID]['description'] = $newConfigArray['configsKeyArray'][$webformID]['form_data'];
                $preJsonReturnArray[$webformID]['description']['start_datetime'] = date("d-m-Y H:i:s", array_values($data['1_1'])[0]->created);
                $preJsonReturnArray[$webformID]['description']['end_datetime'] = date("d-m-Y H:i:s", array_values($data['1_1'])[0]->completed);
                $preJsonReturnArray[$webformID]['description']['user'] = array_values($data['confirmation_box'])[4]->value;
              
                
                foreach($data as $elementID => $element){
                    // print "<h1>---".$elementID." element</h1>";
                    // kint($element);
                
                    foreach($element as  $elementData){
                        // print "<h1>----elementData</h1>";

                        unset($newConfigArray[$elementID]['#flexbox']);
                        unset($newConfigArray[$elementID]['#source__access']);
                        unset($newConfigArray[$elementID]['#title_display']); 
                        
                        
                       // kint(array_values($data['1_1'])[0]->created);

                 
                        $preJsonReturnArray[$webformID]['submissions'][$submissionID][$elementID]['data'][$elementData->property] = $elementData->value;
                        $preJsonReturnArray[$webformID]['submissions'][$submissionID][$elementID]['description'] = $newConfigArray['newConfigArray'][$elementID];
                     
                         
                    
                    }

                    
                    
                        //     $preJsonReturnArray[$webformID][$submissionID] = $data;

                }
                
                // foreach( $webFormData as $webFormData){
                //     $preJsonReturnArray[$webformID] = $webFormData;
                // }
                
            }

            
        }

        //  kint($preJsonReturnArray);


        // foreach($configsKeyArray as $formId => $configKeys){


        //     // if($webFormData[$formId][]){
        //     //     // foreach($configKey as $formQuestionId){
        //     //     //      if($webFormData[$formId]){

        //     //     //      }

        //     //     //     print $formId."<br />";

        //     //     // }
        //     // }

        //     // foreach($configKeys as $configKey){

        //     //     if(is_array($configKey)){

        //     //         foreach($configKey as $formQuestionId){
            
        //     //             foreach($formQuestionId as $questionId => $question){
        //     //                 //print_r($question);
        //     //                 if(is_array($question)){
                    
        //     //                    // kint($webFormData);
                
        //     //                     if(isset($webFormData[$formId])){
        //     //                          $preJsonReturnArray[$formId] = 1;

        //     //                     }
        //     //                 }
        //     //             }
        //     //         }
        //     //     }



        //     // }


        // }

        // print "<h1>Output json</h1>";
        // kint($preJsonReturnArray);
        print json_encode($preJsonReturnArray);

        exit();
    
      }


      function getNewConfigArray($formArray){
        $configSQL = "select * from config where name IN (".implode($formArray, ",").")";	

        $configResults =  \Drupal::database()->query($configSQL)->fetchAll();

       

        $configsKeyArray = array();
        foreach($configResults as $configResult){ 
            $data = unserialize($configResult->data);

            $configsKeyArray[str_replace("webform.webform.","", $configResult->name)]['elements'] = Yaml::parse($data['elements']);
            $configsKeyArray[str_replace("webform.webform.","", $configResult->name)]['form_data']['title'] = $data['title'];
            $configsKeyArray[str_replace("webform.webform.","", $configResult->name)]['form_data']['categories'] = $data['categories'];
        }

        $newConfigArray = array();
        foreach($configsKeyArray['elements']  as $configItems){
           
            foreach( $configItems as $configs){
                foreach($configs as $config){
                    if(is_array($config)){
                        foreach($config as $configKey => $lastConfig){
                            if(is_array($lastConfig)){
                                $newConfigArray[$configKey] = $lastConfig;
                            }
                        }
                    }
                }
            }
          
        }

       
        return array('newConfigArray' => $newConfigArray, 'configsKeyArray' => $configsKeyArray);
      }

}
