<?php

namespace Drupal\fluid_exemplar_webform\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ForceLoginSubscriber.
 */
class ForceLoginSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ForceLoginSubscriber object.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest', 30];

    return $events;
  }

  public function onRequest(Event $event) {
    $current_user = \Drupal::currentUser();
    $current_route = $event->getRequest()->get(RouteObjectInterface::ROUTE_NAME);

    $ignore_routes_anon = array(
      'user.login',
      'user.pass',
      'user.reset',
      'user.reset.form',
      'user.reset.login',
      'entity.user.edit_form',
      'fluid_rest_api.fluid_rest_api_content',
      'tfa.entry'
    );

    if($current_user->isAnonymous() && in_array($current_route, $ignore_routes_anon)===FALSE)
    {
      $response = new RedirectResponse(
        Url::fromRoute(
          'user.login')->toString()
      );
 
      $event->setResponse($response->send()); 
      return;
    }
  }

}
