<?php

namespace Drupal\fluid_exemplar_webform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AsanaSettingsForm.
 */
class AsanaSettingsForm extends ConfigFormBase
{

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'fluid_exemplar_webform.asanasettings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'asana_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('fluid_exemplar_webform.asanasettings');
        $form['personal_access_token'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Personal Access Token'),
            '#description' => $this->t('To generate the token in Asana go to: My profile settings... » Apps » Manage Developer Apps » Personal Access Tokens » Create New Personal Access Token.'),
            '#maxlength' => 255,
            '#size' => 255,
            '#required' => true,
            '#default_value' => $config->get('personal_access_token'),
        ];
        return parent::buildForm($form, $form_state);

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $this->config('fluid_exemplar_webform.asanasettings')
            ->set('personal_access_token', $form_state->getValue('personal_access_token'))
            ->save();
    }

}
