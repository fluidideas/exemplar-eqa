<?php

namespace Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'service_users_peg_element'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("service_users_peg_element")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements\ServiceUsersPegElement
 */
class ServiceUsersPegElement extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo();
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $serviceUsers = SELF::getServiceUsers();
        $elements = [];
        $elements['service_user'] = [
            '#type' => 'select',
            '#title' => t('Service Users'),
            '#options' => $serviceUsers,
        ];

        $elements['peg_support_plans_in_place'] = [
            '#type' => 'radios',
            '#title' => t('Does the service user have a sufficiently detailed and up to date detailed support plans in place?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['peg_support_plans_in_place_action'] = [
            '#type' => 'textarea',
            '#title' => t('What action is being taken?'),
            '#attributes' => [
                'class' => [],
            ],
            '#states' => [
                'visible' => [
                    ':input[name="peg_support_plans_in_place"]' => [
                        'value' => 'no',
                    ],
                ],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];


        $elements['peg_evidence'] = [
            '#type' => 'radios',
            '#title' => t('For PEG is there evidence of cleaning and rotation?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['peg_evidence_action'] = [
            '#type' => 'textarea',
            '#title' => t('What action is being taken?'),
            '#attributes' => [
                'class' => [],
            ],
            '#states' => [
                'visible' => [
                    ':input[name="peg_evidence"]' => [
                        'value' => 'no',
                    ],
                ],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        if ($element['#required'] == true && $element['notApplicable']['#access'] != false) {
            $element['notApplicable']['#access'] = false;
        }
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        //kint($element);
        // '#after_build' => [[get_called_class(), 'afterBuild']],
        switch ($element['#name']) {
            case $composite_name . '[peg_support_plans_in_place_action]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . 'peg_support_plans_in_place"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . 'peg_support_plans_in_place"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[peg_evidence_action]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . 'peg_evidence"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . 'peg_evidence"]' => ['value' => 'no']],
                ];
                break;
        }
        

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function getServiceUsers()
    {
        if(!\Drupal::currentUser()->isAnonymous()){
            $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $userName = $user->getDisplayName();
            $Roles = $user->getRoles();

            $homeTid = $user->field_home->getValue();
            $home = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
            $homeName = $home->name->getValue();

            $HomeID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $homeName[0]['value'], 'vid' => 'service_users']);
            $HomeID = reset($HomeID);

            if($HomeID == null){
                return [];
            }

            $units = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('service_users', $parent = $HomeID->id(), $max_depth = 2, $load_entities = false);
            
            $serviceUsers = [];
            foreach ($units as $unit) {
                if($unit->depth == 0){
                    foreach ($units as $serviceUser) {
                        if($serviceUser->parents[0] == $unit->tid){
                            $serviceUsers[$unit->name][$serviceUser->name] = $serviceUser->name;
                        }
                    }
                }
            }
            return $serviceUsers;
        }
        
        return null;
        
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    
        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;
        $is_NA = (!empty($element['notApplicable']) && $element['notApplicable']['#value'] == true)? true : false;

        $ignored_fields = [
            'section',
            'source',
            'notApplicable',
            'comments'
        ];

        $composite_elements = static::getCompositeElements($element);
        $composite_elements = WebformElementHelper::getFlattened($composite_elements);

        if ($is_element_required == true && $has_access) {
            foreach ($composite_elements as $composite_key => $composite_element) {
                if (!in_array($composite_key, $ignored_fields)) {
                    $is_empty = (isset($value[$composite_key]) && $value[$composite_key] === '');
                    if ($is_empty) {
                        //$form_state->setError($element,'Please fill out any missed data from the form');
                        //WebformElementHelper::setRequiredError($element, $form_state);
                        $element['#attributes']['class'] = ['eqa--required'];
                    }
                }
            }
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }

}
