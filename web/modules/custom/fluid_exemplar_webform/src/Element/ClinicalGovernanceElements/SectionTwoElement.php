<?php

namespace Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'section_two_element'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("section_two_element")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements\SectionTwoElement
 */
class SectionTwoElement extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo();
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];

        $serviceUsers = SELF::getServiceUsers();

        $elements['2_1_1'] = [
            '#type' => 'select',
            '#title' => t('Service Users'),
            '#options' => $serviceUsers,
        ];
        $elements['2_1_2'] = [
            '#type' => 'textarea',
            '#title' => t('Comments'),
            '#attributes' => [
                'class' => [],
            ],
        ];

        $elements['2_2'] = [
            '#type' => 'radios',
            '#title' => t('2.2 Has the home received sufficient preadmission information to support a safe admission of this person'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_2_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken to ensure that we have the right level of information?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_3'] = [
            '#type' => 'radios',
            '#title' => t('2.3 Is the person registered with a GP?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_3_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken to register with GP?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_4'] = [
            '#type' => 'radios',
            '#title' => t('2.4 Does the home have a copy of most recent prescription?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
                'na' => t('N/A'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'three_columns',
        ];

        $elements['2_4_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken to get recent prescription?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_5'] = [
            '#type' => 'radios',
            '#title' => t('2.5 Have the medications been added to the EMAR/MAR chart and countersigned?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_5_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken to add to the MAR chart?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_6'] = [
            '#type' => 'radios',
            '#title' => t('2.6 Have the medication been ordered from the GP?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_6_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken to order medication?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_7'] = [
            '#type' => 'radios',
            '#title' => t('2.7 Does the home need to arrange an interim script to sync medication into the units cycle dates?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_7_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken to obtain an interim script"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_8'] = [
            '#type' => 'radios',
            '#title' => t('2.8 Have the appropriate medication care plans been written (including PRN)'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_8_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_9'] = [
            '#type' => 'radios',
            '#title' => t('2.9 Has the home received all medication needed for the new admission?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_9_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_10'] = [
            '#type' => 'radios',
            '#title' => t('2.10 Are any medications prescribed covertly?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_10_1'] = [
            '#type' => 'radios',
            '#title' => t('2.10.1 Have appropriate covert medication MCA/BID and authorisations been completed?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_10_1_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_11'] = [
            '#type' => 'radios',
            '#title' => t('2.11 Has the new admission audit been completed?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_11_1'] = [
            '#type' => 'textarea',
            '#title' => t("Please add scores and further information?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_11_2'] = [
            '#type' => 'textarea',
            '#title' => t("what action is being taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_11_3'] = [
            '#type' => 'radios',
            '#title' => t('2.11.3 Has this been checked and signed off by a manager?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_11_3_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_12'] = [
            '#type' => 'radios',
            '#title' => t('2.12 Have support plans been reviewed and updated?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_12_1'] = [
            '#type' => 'textarea',
            '#title' => t("What action is being taken?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_13'] = [
            '#type' => 'radios',
            '#title' => t('2.13 Is there a requirement for MDT support?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['2_13_1'] = [
            '#type' => 'radios',
            '#title' => t('2.13.1 Are any planned MDT meetings?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_13_1_1'] = [
            '#type' => 'textarea',
            '#title' => t("What dates and who is invited?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_13_2'] = [
            '#type' => 'radios',
            '#title' => t('2.13.2 Are any referrals needed?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['2_13_2_1'] = [
            '#type' => 'textarea',
            '#title' => t("Referrals to where and when?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        if ($element['#required'] == true && $element['notApplicable']['#access'] != false) {
            $element['notApplicable']['#access'] = false;
        }
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        // '#after_build' => [[get_called_class(), 'afterBuild']],
        switch ($element['#name']) {
            case $composite_name . '[2_2_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_2]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_2]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_2]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_3_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_3]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_4_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_4]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_5_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_5]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_5]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_5]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_6_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_6]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_6]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_6]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_7_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_7]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_7]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_7]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[2_8_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_8]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_8]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_8]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_9_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_9]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_9]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_9]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_10_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_10]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_10]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_10]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[2_10_1_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_10_1]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_10_1]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_10_1]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_11_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[2_11_2]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_11_3]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_11]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[2_11_3_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_11_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_11_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_11_3]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_12_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_12]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_12]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_12]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[2_13_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_13]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_13]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_13]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[2_13_1_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_13_1]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_13_1]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_13_1]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[2_13_2]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_13]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_13]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_13]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[2_13_2_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[2_13_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[2_13_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[2_13_2]"]' => ['value' => 'yes']],
                ];
                break;
        }

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function getServiceUsers()
    {
        if (!\Drupal::currentUser()->isAnonymous()) {
            $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $userName = $user->getDisplayName();
            $Roles = $user->getRoles();

            $homeTid = $user->field_home->getValue();
            $home = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
            $homeName = $home->name->getValue();

            $HomeID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $homeName[0]['value'], 'vid' => 'service_users']);
            $HomeID = reset($HomeID);

            if ($HomeID == null) {
                return [];
            }

            $units = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('service_users', $parent = $HomeID->id(), $max_depth = 2, $load_entities = false);

            $serviceUsers = [];
            foreach ($units as $unit) {
                if ($unit->depth == 0) {
                    foreach ($units as $serviceUser) {
                        if ($serviceUser->parents[0] == $unit->tid) {
                            $serviceUsers[$unit->name][$serviceUser->name] = $serviceUser->name;
                        }
                    }
                }
            }
            return $serviceUsers;
        }

        return null;

    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);

        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;
        $is_NA = (!empty($element['notApplicable']) && $element['notApplicable']['#value'] == true) ? true : false;

        $ignored_fields = [
            'section',
            'source',
            'notApplicable',
            'comments',
        ];

        $composite_elements = static::getCompositeElements($element);
        $composite_elements = WebformElementHelper::getFlattened($composite_elements);

        if ($is_element_required == true && $has_access) {
            foreach ($composite_elements as $composite_key => $composite_element) {
                if (!in_array($composite_key, $ignored_fields)) {
                    $is_empty = (isset($value[$composite_key]) && $value[$composite_key] === '');
                    if ($is_empty) {
                        //$form_state->setError($element,'Please fill out any missed data from the form');
                        //WebformElementHelper::setRequiredError($element, $form_state);
                        $element['#attributes']['class'] = ['eqa--required'];
                    }
                }
            }
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }

}
