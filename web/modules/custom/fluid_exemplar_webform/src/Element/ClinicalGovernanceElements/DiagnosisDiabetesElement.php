<?php

namespace Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'diagnosis_diabetes_element'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("diagnosis_diabetes_element")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements\DiagnosisDiabetesElement
 */
class DiagnosisDiabetesElement extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo();
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $serviceUsers = SELF::getServiceUsers();
        $elements = [];
        $elements['service_user'] = [
            '#type' => 'select',
            '#title' => t('Service Users'),
            '#options' => $serviceUsers,
        ];

        $elements['diabetes_profiles_complete'] = [
            '#type' => 'radios',
            '#title' => t('Are the diabetic profiles complete and up to date?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['diabetes_profiles_complete_action'] = [
            '#type' => 'textarea',
            '#title' => t('What action is being taken?'),
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['diabetes_support_plans'] = [
            '#type' => 'radios',
            '#title' => t('Are the diabetic support plans up to date?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['diabetes_support_plans_action'] = [
            '#type' => 'textarea',
            '#title' => t('What action is being taken?'),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['insulin_dependent'] = [
            '#type' => 'radios',
            '#title' => t('Is the service user insulin dependent?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['insulin_administration'] = [
            '#type' => 'select',
            '#title' => t('Is there a body map in place for insulin administration?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#options_display' => 'two_columns',
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['insulin_administration_action'] = [
            '#type' => 'textarea',
            '#title' => t('What action is being taken?'),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        // if ($element['#required'] == true && $element['notApplicable']['#access'] != false) {
        //     $element['notApplicable']['#access'] = false;
        // }
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        // kint($element);
        // $element['#states']['disabled'] = [
        //     [':input[name="' . $composite_name . '[notApplicable]"]' => ['checked' => true]],
        // ];

        switch ($element['#name']) {
            case $composite_name . '[diabetes_profiles_complete_action]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[diabetes_profiles_complete]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[diabetes_profiles_complete]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[diabetes_support_plans_action]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[diabetes_support_plans]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[diabetes_support_plans]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[insulin_administration]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[insulin_dependent]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[insulin_dependent]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[insulin_administration_action]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[insulin_administration]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[insulin_administration]"]' => ['value' => 'no']],
                ];
                break;
        }

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function getServiceUsers()
    {
        if (!\Drupal::currentUser()->isAnonymous()) {
            $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $userName = $user->getDisplayName();
            $Roles = $user->getRoles();

            $homeTid = $user->field_home->getValue();
            $home = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
            $homeName = $home->name->getValue();

            $HomeID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $homeName[0]['value'], 'vid' => 'service_users']);
            $HomeID = reset($HomeID);

            if ($HomeID == null) {
                return [];
            }

            $units = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('service_users', $parent = $HomeID->id(), $max_depth = 2, $load_entities = false);

            $serviceUsers = [];
            foreach ($units as $unit) {
                if ($unit->depth == 0) {
                    foreach ($units as $serviceUser) {
                        if ($serviceUser->parents[0] == $unit->tid) {
                            $serviceUsers[$unit->name][$serviceUser->name] = $serviceUser->name;
                        }
                    }
                }
            }
            return $serviceUsers;
        }

        return null;

    }

}
