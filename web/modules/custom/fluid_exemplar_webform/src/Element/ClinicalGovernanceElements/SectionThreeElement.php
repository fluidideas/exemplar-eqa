<?php

namespace Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'section_three_element'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("section_three_element")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements\SectionThreeElement
 */
class SectionThreeElement extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'SectionThreeElement'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];

        $serviceUsers = SELF::getServiceUsers();

        $form['3_1_1'] = [
            '#type' => 'fieldset',
            '#title' => 'group',
            '#collapsible' => false,
            '#collapsed' => false,
        ];

        $elements['3_1_1']['service_user'] = [
            '#type' => 'select',
            '#title' => t('Service Users'),
            '#options' => $serviceUsers,
        ];

        $elements['3_1_1']['type'] = [
            '#type' => 'textfield',
            '#title' => t('Type'),
        ];

        $elements['3_1_1']['location'] = [
            '#type' => 'textfield',
            '#title' => t('Location'),
        ];

        $elements['3_2'] = [
            '#type' => 'radios',
            '#title' => t('3.2 Is this a newly acquired wound?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['3_2_1'] = [
            '#type' => 'radios',
            '#title' => t('Has the wound been reported to Safeguarding?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_2'] = [
            '#type' => 'radios',
            '#title' => t('Does this wound meet the criteria for notification to other statutory agencies?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_2_1'] = [
            '#type' => 'webform_select_other',
            '#title' => t('Please state which agency(ies) have been notified.'),
            '#other' => t('Other (please Specify)'),
            '#options' => [
                'CQC',
                'Commissioners',
                'Safeguarding',
                'CCG',
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_3'] = [
            '#type' => 'radios',
            '#title' => t('Has an RCA been completed?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_3_1'] = [
            '#type' => 'date',
            '#title' => 'Date Completed',
            '#date_date_format' => '',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_3_2'] = [
            '#type' => 'textarea',
            '#title' => t("why RCA has not been completed"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_3_3'] = [
            '#type' => 'radios',
            '#title' => t('Are there any actions to complete from RCA?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_3_3_1'] = [
            '#type' => 'textarea',
            '#title' => t("What actions and date due to be completed"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_4'] = [
            '#type' => 'radios',
            '#title' => t('Has the person’s family been informed of the wound'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_4_1'] = [
            '#type' => 'date',
            '#title' => 'Date Informed',
            '#date_date_format' => '',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_2_4_2'] = [
            '#type' => 'textarea',
            '#title' => t("why not?"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],

        ];

        $elements['3_3'] = [
            '#type' => 'radios',
            '#title' => t('3.3 Do care plans reflect the current wound status and TVN advice'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['3_3_1'] = [
            '#type' => 'textarea',
            '#title' => t("Please explain why and when care plans will be updated"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_4'] = [
            '#type' => 'radios',
            '#title' => t('3.4 Do wound care evaluations evidence treatment is being carried out in accordance with the treatment plan reflect planned dressing changes (staff are completing dressings and changes as planned)'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['3_4_1'] = [
            '#type' => 'textarea',
            '#title' => t("Please explain why and when evaluations will be carried out"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_5'] = [
            '#type' => 'radios',
            '#title' => t('3.5 Is the wound healing as expected with the current prescribed treatment?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['3_5_1'] = [
            '#type' => 'textarea',
            '#title' => t("what action is being taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_6'] = [
            '#type' => 'radios',
            '#title' => t('3.6 Is the wound showing signs of infection or deterioration?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['3_6_1'] = [
            '#type' => 'textarea',
            '#title' => t("what action is being taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_7'] = [
            '#type' => 'radios',
            '#title' => t('3.7 Has the wound been referred to / need a referral to another specialist?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['3_7_1'] = [
            '#type' => 'select',
            '#title' => t('Referred too'),
            '#options' => [
                'GP' => 'GP',
                'TVN' => 'TVN',
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_8'] = [
            '#type' => 'radios',
            '#title' => t('3.8 Are positional change charts being completed in full, in a timely manner as per support plan with frequency matching timings on the support plan?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
                'na' => t('N/A'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'three_columns',
        ];

        $elements['3_8_1'] = [
            '#type' => 'textarea',
            '#title' => t("Why and what action is being taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_9'] = [
            '#type' => 'radios',
            '#title' => t('3.9 Is the correct pressure relieving equipment in place?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
                'na' => t('N/A'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'three_columns',
        ];

        $elements['3_9_1'] = [
            '#type' => 'textarea',
            '#title' => t("Why and what action is being taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['3_10'] = [
            '#type' => 'radios',
            '#title' => t('3.10 Is there evidence of mattress settings being correct and reviewed regularly?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
                'na' => t('N/A'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'three_columns',
        ];

        $elements['3_10_1'] = [
            '#type' => 'textarea',
            '#title' => t("Why and what action is being taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        if ($element['#required'] == true && $element['notApplicable']['#access'] != false) {
            $element['notApplicable']['#access'] = false;
        }
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        // '#after_build' => [[get_called_class(), 'afterBuild']],
        switch ($element['#name']) {
            case $composite_name . '[3_2_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_2_2]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_2_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_2]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_2_1_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_2_1]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_2_1]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_2_1]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_3]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_3_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_3_2]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_2_3_3]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_3]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_3_3_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_3_3]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_3_3]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_3_3]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_4]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_4_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_4]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_4]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_4]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_2_4_2]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_2_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_2_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_2_4]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_3_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_3]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_4_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_4]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_5_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_5]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_5]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_5]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_6_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_6]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_6]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_6]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_7_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_7]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_7]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_7]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[3_8_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_8]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_8]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_8]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_9_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_9]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_9]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_9]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[3_10_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[3_10]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[3_10]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[3_10]"]' => ['value' => 'no']],
                ];
                break;
        }

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function getServiceUsers()
    {
        if (!\Drupal::currentUser()->isAnonymous()) {
            $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $userName = $user->getDisplayName();
            $Roles = $user->getRoles();

            $homeTid = $user->field_home->getValue();
            $home = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
            $homeName = $home->name->getValue();

            $HomeID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $homeName[0]['value'], 'vid' => 'service_users']);
            $HomeID = reset($HomeID);

            if ($HomeID == null) {
                return [];
            }

            $units = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('service_users', $parent = $HomeID->id(), $max_depth = 2, $load_entities = false);

            $serviceUsers = [];
            foreach ($units as $unit) {
                if ($unit->depth == 0) {
                    foreach ($units as $serviceUser) {
                        if ($serviceUser->parents[0] == $unit->tid) {
                            $serviceUsers[$unit->name][$serviceUser->name] = $serviceUser->name;
                        }
                    }
                }
            }
            return $serviceUsers;
        }

        return null;

    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);

        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;
        $is_NA = (!empty($element['notApplicable']) && $element['notApplicable']['#value'] == true) ? true : false;

        $ignored_fields = [
            'section',
            'source',
            'notApplicable',
            'comments',
        ];

        $composite_elements = static::getCompositeElements($element);
        $composite_elements = WebformElementHelper::getFlattened($composite_elements);

        if ($is_element_required == true && $has_access) {
            foreach ($composite_elements as $composite_key => $composite_element) {
                if (!in_array($composite_key, $ignored_fields)) {
                    $is_empty = (isset($value[$composite_key]) && $value[$composite_key] === '');
                    if ($is_empty) {
                        //$form_state->setError($element,'Please fill out any missed data from the form');
                        //WebformElementHelper::setRequiredError($element, $form_state);
                        $element['#attributes']['class'] = ['eqa--required'];
                    }
                }
            }
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }

}
