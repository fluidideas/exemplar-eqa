<?php

namespace Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'section_four_element'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("section_four_element")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements\SectionFourElement
 */
class SectionFourElement extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo();
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];

        $serviceUsers = SELF::getServiceUsers();

        $elements['staff'] = [
            '#type' => 'radios',
            '#title' => t('Staff'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['comments'] = [
            '#type' => 'textarea',
            '#title' => t("Comments"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['service_user'] = [
            '#type' => 'radios',
            '#title' => t('Service Users'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['service_user_name'] = [
            '#type' => 'select',
            '#title' => t('Service Users'),
            '#options' => $serviceUsers,
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['service_user_incident'] = [
            '#type' => 'select',
            '#title' => t('Type of Incident'),
            '#options' => [
                'fall' => 'fall',
                'Injury' => 'Injury',
                'restraint' => 'restraint',
                'altercation' => 'altercation',
                'choke' => 'choke',
                'death' => 'death',
                'other' => 'other'
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_2'] = [
            '#type' => 'radios',
            '#title' => t('4.2 Have the incidents been reviewed by a manager?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_2_1'] = [
            '#type' => 'textarea',
            '#title' => t("Action taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_3'] = [
            '#type' => 'radios',
            '#title' => t('4.3 Has a Root- Cause Analysis been completed?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_3_1'] = [
            '#type' => 'textarea',
            '#title' => t("Action taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_4'] = [
            '#type' => 'radios',
            '#title' => t('4.4 Has the person’s care plan been updated to reflect changes?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_4_1'] = [
            '#type' => 'textarea',
            '#title' => t("Action taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_5'] = [
            '#type' => 'radios',
            '#title' => t('4.5 Have referrals been made to relevant external agencies?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_5_1'] = [
            '#type' => 'select',
            '#title' => t('Referred too'),
            '#options' => [
                'GP' => 'GP',
                'Safeguarding' => 'Safeguarding',
                'CCG' => 'CCG',
                'CQC' => 'CQC',
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_6'] = [
            '#type' => 'radios',
            '#title' => t('4.6 Has family been notified?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_6_1'] = [
            '#type' => 'textarea',
            '#title' => t("who"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_6_2'] = [
            '#type' => 'textarea',
            '#title' => t("why"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_7'] = [
            '#type' => 'radios',
            '#title' => t('4.7 Is support from internal central support services required?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_7_1'] = [
            '#type' => 'select',
            '#title' => t('Support Required'),
            '#options' => [
                'BHS' => 'BHS',
                'Clinical' => 'Clinical',
                'MH' => 'MH',
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_8'] = [
            '#type' => 'radios',
            '#title' => t('4.8 Is there evidence of lessons learned and change of practice in response to the incidents?'),
            '#options' => [
                'yes' => t('Yes'),
                'no' => t('No'),
            ],
            '#attributes' => [
                'class' => ['subset_title'],
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['4_8_1'] = [
            '#type' => 'textarea',
            '#title' => t("actions taken / to be taken"),
            '#attributes' => [
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        if ($element['#required'] == true && $element['notApplicable']['#access'] != false) {
            $element['notApplicable']['#access'] = false;
        }
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        // '#after_build' => [[get_called_class(), 'afterBuild']],
        switch ($element['#name']) {
            case $composite_name . '[comments]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[staff]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[staff]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[staff]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[service_user_name]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[service_user_incident]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_2]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_2_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_2]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_2]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_2]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[4_3]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_3_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_3]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_3]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[4_4]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_4_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_4]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_4]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[4_5]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_5_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_5]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_5]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_5]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_6]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_6_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_6]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_6]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_6]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_6_2]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_6]"]' => ['value' => 'no']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_6]"]' => ['value' => 'no']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_6]"]' => ['value' => 'no']],
                ];
                break;
            case $composite_name . '[4_7]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_7_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_7]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_7]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_7]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_8]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[service_user]"]' => ['value' => 'yes']],
                ];
                break;
            case $composite_name . '[4_8_1]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[4_8]"]' => ['value' => 'yes']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[4_8]"]' => ['value' => 'yes']],
                ];
                $element['#states']['enabled'] = [
                    [':input[name="' . $composite_name . '[4_8]"]' => ['value' => 'yes']],
                ];
                break;
        }

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function getServiceUsers()
    {
        if (!\Drupal::currentUser()->isAnonymous()) {
            $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $userName = $user->getDisplayName();
            $Roles = $user->getRoles();

            $homeTid = $user->field_home->getValue();
            $home = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
            $homeName = $home->name->getValue();

            $HomeID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $homeName[0]['value'], 'vid' => 'service_users']);
            $HomeID = reset($HomeID);

            if ($HomeID == null) {
                return [];
            }

            $units = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('service_users', $parent = $HomeID->id(), $max_depth = 2, $load_entities = false);

            $serviceUsers = [];
            foreach ($units as $unit) {
                if ($unit->depth == 0) {
                    foreach ($units as $serviceUser) {
                        if ($serviceUser->parents[0] == $unit->tid) {
                            $serviceUsers[$unit->name][$serviceUser->name] = $serviceUser->name;
                        }
                    }
                }
            }
            return $serviceUsers;
        }

        return null;
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);

        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;
        $is_NA = (!empty($element['notApplicable']) && $element['notApplicable']['#value'] == true) ? true : false;

        $ignored_fields = [
            'section',
            'source',
            'notApplicable',
            'comments',
        ];

        $composite_elements = static::getCompositeElements($element);
        $composite_elements = WebformElementHelper::getFlattened($composite_elements);

        if ($is_element_required == true && $has_access) {
            foreach ($composite_elements as $composite_key => $composite_element) {
                if (!in_array($composite_key, $ignored_fields)) {
                    $is_empty = (isset($value[$composite_key]) && $value[$composite_key] === '');
                    if ($is_empty) {
                        //$form_state->setError($element,'Please fill out any missed data from the form');
                        //WebformElementHelper::setRequiredError($element, $form_state);
                        $element['#attributes']['class'] = ['eqa--required'];
                    }
                }
            }
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }
}
