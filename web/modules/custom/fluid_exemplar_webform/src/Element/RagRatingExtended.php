<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'rag_rating_extended'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("rag_rating_extended")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\RagRatingExtended
 */
class RagRatingExtended extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'rag_rating_extended'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];

        $elements['section'] = [
            '#type' => 'item',
            '#title' => t('Section'),
            '#attributes' => ['readonly' => 'readonly'],
        ];

        $elements['rag_blue'] = [
            '#type' => 'textfield',
            '#title' => t('Blue <span>(90% and above)</span>'),
            '#attributes' => [
                'readonly' => 'readonly',
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['rag_green'] = [
            '#type' => 'textfield',
            '#title' => t('Green <span>(80% - 89%)</span>'),
            '#attributes' => [
                'readonly' => 'readonly',
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['rag_amber'] = [
            '#type' => 'textfield',
            '#title' => t('Amber <span>(66% - 79%)</span>'),
            '#attributes' => [
                'readonly' => 'readonly',
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['rag_red'] = [
            '#type' => 'textfield',
            '#title' => t('Red <span>(65% and less)</span>'),
            '#attributes' => [
                'readonly' => 'readonly',
                'class' => [],
            ],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {

        $element = parent::preRenderWebformCompositeFormElement($element);
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match_all("/\\[(.*?)\\]/", $element['#name'], $match);
        $composite_name = $match[1];

        $url = \Drupal::service('path.current')->getPath();
        $currentPath = strpos($url, '/print/pdf/');

        $formFields = $form_state->getFormObject()->getWebform()->getElementsInitializedFlattenedAndHasValue();

        //populating Rag extended Totals fields
        if (!empty($form_state->getFormObject()->getWebform()->getElement('grand_total'))) {
            $elementValue = $form_state->getValue('grand_total');

            $percentageValue = round($elementValue['percentage'], 2);

            if (isset($percentageValue) && $percentageValue >= 0) {
                switch ($composite_name[0]) {
                    case 'rag_blue':
                        if ($percentageValue > 89) {
                            $element['#value'] = $percentageValue;
                            $element['#attributes']['class'][] = 'rag_blue_true';
                        }
                        break;
                    case 'rag_green':
                        if ($percentageValue <= 89 && $percentageValue >= 80) {
                            $element['#value'] = $percentageValue;
                            $element['#attributes']['class'][] = 'rag_green_true';
                        }
                        break;
                    case 'rag_amber':
                        if ($percentageValue <= 79 && $percentageValue >= 66) {
                            $element['#value'] = $percentageValue;
                            $element['#attributes']['class'][] = 'rag_amber_true';
                        }
                        break;
                    case 'rag_red':
                        if ($percentageValue <= 65) {
                            $element['#value'] = $percentageValue;
                            $element['#attributes']['class'][] = 'rag_red_true';
                        }
                        break;
                }
            }
        }

        //$element['#attributes']['class'][] = 'bob';
        $element['#states']['disabled'] = [];

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';

        return $element;
    }
}
