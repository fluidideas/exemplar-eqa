<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'asana_action'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("asana_action")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\AsanaAction
 */
class AsanaAction extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'asana_action'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];

        $elements['comments'] = [
            '#type' => 'textarea',
            '#title' => t('Action Points'),
        ];

        return $elements;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        //kint($element);

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        $is_element_required = (isset($element['#_required']) && $element['#_required'] == true) ? true : false;
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
        $url = \Drupal::service('path.current')->getPath();
        $current_path = strpos($url, '/print/pdf/');

        if ($current_path === false && strpos($url, '/viewSubmmission/') === false) {
            if ($is_element_required && empty($value['comments'])) {
                $form_state->setError($element, 'Please fill out any missed data from the form');
                //WebformElementHelper::setRequiredError($element, $form_state);
                $element['#attributes']['class'] = ['eqa--required'];
            }

            // Clear empty composites value.
            if (empty(array_filter($value))) {
                $element['#value'] = null;
                $form_state->setValueForElement($element, null);
            }
        }

    }

}
