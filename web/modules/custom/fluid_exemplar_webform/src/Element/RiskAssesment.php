<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'risk_assesment'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("risk_assesment")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\RiskAssesment
 */
class RiskAssesment extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'risk_assesment'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];
        $elements['audit_section'] = [
            '#type' => 'item',
            '#title' => t('Section'),
        ];

        $elements['last_inspection'] = [
            '#type' => 'date',
            '#title' => t('Date of last inspection/visit'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],
            ],
        ];

        $elements['oustanding_action'] = [
            '#type' => 'textfield',
            '#title' => t('Oustanding actions (if applicable)'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],
            ],
        ];

        $elements['progress_made'] = [
            '#type' => 'textfield',
            '#title' => t('Action undertaken/progress made since inspection/visit'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],
            ],
        ];

        $elements['target_date'] = [
            '#type' => 'date',
            '#title' => t('Date'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],
            ],
        ];

        $elements['assigned_too'] = [
            '#type' => 'textfield',
            '#title' => t('Assigned to:'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],  
            ],
        ];

        $elements['added_to_ihap'] = [
            '#type' => 'textfield',
            '#title' => t('Added to IHAP:'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],
            ],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        //kint($element);
        $element['#states']['disabled'] = [
            [':input[name="' . $composite_name . '[notApplicable]"]' => ['checked' => true]],
        ];

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    
        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;

        $ignored_fields = [
            'section',
            'source',
        ];

        if ($has_access) {
            // Validate required composite elements.
            $composite_elements = static::getCompositeElements($element);
            $composite_elements = WebformElementHelper::getFlattened($composite_elements);
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }

}
