<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'service_users'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("service_users")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\ServiceUsers
 */
class ServiceUsers extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'service_users'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $serviceUsers = SELF::getServiceUsers();
        for ($sampleUsers = 1; $sampleUsers <= 10; $sampleUsers++) {

            $elements['service_user_'. $sampleUsers .'_initials'] = [
                '#type' => 'select',
                '#title' => t('Service Users'),
                '#options' => $serviceUsers,
                '#after_build' => [[get_called_class(), 'afterBuild']],
            ];
        }
        return $elements;
    }

    public static function getServiceUsers()
    {
        if(!\Drupal::currentUser()->isAnonymous()){
            $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $userName = $user->getDisplayName();
            $Roles = $user->getRoles();

            $homeTid = $user->field_home->getValue();
            $home = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
            $homeName = $home->name->getValue();

            $HomeID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $homeName[0]['value'], 'vid' => 'service_users']);
            $HomeID = reset($HomeID);

            if($HomeID == null){
                return [];
            }

            $units = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('service_users', $parent = $HomeID->id(), $max_depth = 2, $load_entities = false);
            
            $serviceUsers = [];
            foreach ($units as $unit) {
                if($unit->depth == 0){
                    foreach ($units as $serviceUser) {
                        if($serviceUser->parents[0] == $unit->tid){
                            $serviceUsers[$unit->name][$serviceUser->name] = $serviceUser->name;
                        }
                    }
                }
            }
            return $serviceUsers;
        }
                    
        return null;
    }

    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the 
        //radio options 'NA' so it has to be completed on the form
        return $element;
    }   

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        //kint($element);
        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
       // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    
        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;
        $is_NA = (!empty($element['notApplicable']) && $element['notApplicable']['#value'] == true)? true : false;

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }

}
