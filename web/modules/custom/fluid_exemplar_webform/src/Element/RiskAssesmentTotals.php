<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'risk_assesment_totals'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("risk_assesment_totals")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\RiskAssesmentTotals
 */
class RiskAssesmentTotals extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'risk_assesment_totals'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];

        $elements['form_information'] = [
            '#type' => 'textarea',
            '#title' => t('Form Information'),
            '#attributes' => [
                'class' => ['form--info__content']
            ]
        ];

        $elements['audit_section'] = [
            '#type' => 'item',
            '#title' => t('Section'),
        ];

        $elements['initial_score'] = [
            '#type' => 'number',
            '#title' => t('Initial score'),
            '#attributes' => [
                'class' => [],
            ],
        ];

        $elements['review_score'] = [
            '#type' => 'number',
            '#title' => t('Review score'),
            '#attributes' => [
                'class' => [],
            ],
        ];

        $elements['action_required'] = [
            '#type' => 'radios',
            '#title' => t('<span>Corrective actions required?</span>'),
            '#options' => [
                'pass' => t('Yes'),
                'fail' => t('No'),
            ],
            '#attributes' => [
                'data-subtext' => [t('(If yes, please complete section 4)')],
            ],
            '#options_display' => 'two_columns',
        ];

        return $elements;
    }

}
