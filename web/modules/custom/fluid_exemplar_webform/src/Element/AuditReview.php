<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'audit_review'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("audit_review")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\AuditReview
 */
class AuditReview extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'audit_review'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];

        $elements['house_name'] = [
            '#type' => 'textfield',
            '#title' => t('House Name'),
            '#attributes' => ['readonly' => 'readonly'],
        ];

        $elements['user_name'] = [
            '#type' => 'textfield',
            '#title' => t('Person completing review'),
            '#attributes' => ['readonly' => 'readonly'],
        ];

        $elements['date'] = [
            '#type' => 'textfield',
            '#title' => t('Date'),
            '#attributes' => ['readonly' => 'readonly'],
        ];

        $elements['role'] = [
            '#type' => 'textfield',
            '#title' => t('Role\'s'),
            '#attributes' => ['readonly' => 'readonly'],
        ];

        $elements['action'] = [
            '#type' => 'radios',
            '#title' => t('Pass/Fail'),
            '#options' => [
                'accept' => 'Accept',
                'action_required' => 'Action Required',
            ],
            '#options_display' => 'two_columns',
        ];

        $elements['review_asana_action'] = [
            '#type' => 'textarea',
            '#title' => t('Action'),
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $fieldValues = SELF::getFeildValues();
        $element = parent::preRenderWebformCompositeFormElement($element);
        if ($element['user_name']['#value'] == "") {
            $element['user_name']['#value'] = $fieldValues['user_name'];
        }
        if ($element['house_name']['#value'] == "") {
            $element['house_name']['#value'] = $fieldValues['house_name'];
        }
        if ($element['date']['#value'] == "") {
            $element['date']['#value'] = $fieldValues['date'];
        }
        if ($element['role']['#value'] == "") {
            $element['role']['#value'] = $fieldValues['role'];
        }
        $element['user_name'];
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];

        switch ($element['#name']) {
            case $composite_name . '[review_asana_action]':
                $element['#states']['visible'] = [
                    [':input[name="' . $composite_name . '[action]"]' => ['value' => 'action_required']],
                ];
                $element['#states']['required'] = [
                    [':input[name="' . $composite_name . '[action]"]' => ['value' => 'action_required']],
                ];
                break;
        }

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';

        return $element;
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);

        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $has_confirmed = (!empty($element['action']['#value']));

        if ($has_access) {
            if (!$has_confirmed) {
                $form_state->setError($element, 'Please Confirm before completing form');
                //WebformElementHelper::setRequiredError($element, $form_state);
                $element['#attributes']['class'] = ['eqa--required'];
            }
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }

    public static function getFeildValues()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $userName = (!empty($user->field_name->getValue())) ? $user->field_name->getValue() : $user->getDisplayName();
        $Roles = $user->getRoles();
        $houseName = 'house 1';

        $homeTid = $user->field_home->getValue();
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
        $homeName = $term->name->value;
        $ignoredRoles = [
            'administrator',
            'authenticated',
        ];

        foreach ($Roles as $role) {
            if (!in_array($role, $ignoredRoles)) {
                if (empty($userRole)) {
                    $userRole = $role;
                } else {
                    $userRole = $userRole . ', ' . $role;
                }

            }
        }

        $userRole = (isset($userRole)) ? $userRole : '';
        $fieldValues = [
            'house_name' => $homeName,
            'user_name' => (is_array($userName)) ? $userName[0]['value'] : $userName,
            'date' => date("d/m/Y"),
            'role' => ucwords($userRole),
        ];
        if (in_array('head_office', $Roles) || in_array('home_manager', $Roles) || in_array('regional_director_of_operations', $Roles) || in_array('administrator', $Roles)) {
            return $fieldValues;
        }

        return [
            'house_name' => '',
            'user_name' => '',
            'date' => '',
            'role' => '',
        ];

    }

}
