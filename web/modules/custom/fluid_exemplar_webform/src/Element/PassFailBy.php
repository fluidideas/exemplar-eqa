<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'pass_fail_by'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("pass_fail_by")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\PassFailBy
 */
class PassFailBy extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'pass_fail_by'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];
        $elements['section'] = [
            '#type' => 'item',
            '#title' => t('Section'),
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['source'] = [
            '#type' => 'item',
            '#title' => t('Source'),
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['notApplicable'] = [
            '#type' => 'checkbox',
            '#title' => 'N/A',
            '#attributes' => [
                'class' => [
                    'checkbox_na',
                ],
            ],
            // Use #after_build to add #states.
        ];
        $elements['overallPassFail'] = [
            '#type' => 'radios',
            '#title' => t('Pass/Fail'),
            '#options' => [
                'pass' => t('Pass'),
                'fail' => t('Fail'),
            ],
            '#options_display' => 'two_columns',
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['comments'] = [
            '#type' => 'textarea',
            '#title' => t('Comments'),
        ];

        $elements['by_whom'] = [
            '#type' => 'textfield',
            '#title' => t('Name of person escalated to:'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],
            ],
        ];

        $elements['by_when'] = [
            '#type' => 'date',
            '#title' => t('Date'),
            '#attributes' => [
                // 'readonly' => 'readonly',
                'class' => [],
            ],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        if ($element['#required'] == true && $element['notApplicable']['#access'] != false) {
            $element['notApplicable']['#access'] = false;
        }
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        //kint($element);
        $element['#states']['disabled'] = [
            [':input[name="' . $composite_name . '[notApplicable]"]' => ['checked' => true]],
        ];

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    
        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;
        $is_NA = (!empty($element['notApplicable']) && $element['notApplicable']['#value'] == true)? true : false;

        $ignored_fields = [
            'section',
            'source',
            'notApplicable',
            'comments',
            'by_whom',
            'by_when'
        ];

        if ($has_access) {
            // Validate required composite elements.
            $composite_elements = static::getCompositeElements($element);
            $composite_elements = WebformElementHelper::getFlattened($composite_elements);
            if ($is_element_required == true || $is_NA == false) {
                foreach ($composite_elements as $composite_key => $composite_element) {
                    if (!in_array($composite_key,$ignored_fields)) {
                        $is_empty = (isset($value[$composite_key]) && $value[$composite_key] === '');
                        if ($is_empty) {
                            $form_state->setError($element,'Please fill out any missed data from the form');
                            //  WebformElementHelper::setRequiredError($element, $form_state);
                            $element['#attributes']['class'] = ['eqa--required'];
                        }
                    }
                }
            } elseif ($is_NA == true) { 
                $is_empty = (isset($value['comments']) && $value['comments'] === '');
                if ($is_empty) {
                    $form_state->setError($element,'Please fill out any missed data from the form');
                    //   WebformElementHelper::setRequiredError($element, $form_state);
                    $element['#attributes']['class'] = ['eqa--required'];
                }
           }
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }

}
