<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'grand_total'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("grand_total")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\GrandTotal
 */
class GrandTotal extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'grand_total'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];
        $elements['section'] = [
            '#type' => 'item',
            '#title' => t('Grand total scores'),
            '#attributes' => ['readonly' => 'readonly'],
        ];

        $elements['maximum'] = [
            '#type' => 'textfield',
            '#title' => t('Maximum'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['possible'] = [
            '#type' => 'textfield',
            '#title' => t('Possible'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['actual'] = [
            '#type' => 'textfield',
            '#title' => t('Actual'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['percentage'] = [
            '#type' => 'textfield',
            '#title' => t('%'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];

        $url = \Drupal::service('path.current')->getPath();
        $currentPath = strpos($url, '/print/pdf/');

        if ($currentPath === false && strpos($url, '/viewSubmmission/') === false) {
            $formFields = $form_state->getFormObject()->getWebform()->getElementsInitializedFlattenedAndHasValue();
            //populating Grandtotal fields
            $grandTotalMax = 0;
            $grandTotalPoss = 0;
            $grandTotalActual = 0;

            foreach ($formFields as $field) {
                if ($field['#type'] == 'page_pass_fail_total') {
                    $elementValue = $form_state->getValue($field['#webform_key']);
                    $grandTotalMax = $grandTotalMax + $elementValue['maximum'];
                    $grandTotalPoss = $grandTotalPoss + $elementValue['possible'];
                    $grandTotalActual = $grandTotalActual + $elementValue['actual'];
                }
            }

            //adds field values to the element
            switch ($element['#name']) {
                case $composite_name . '[maximum]':
                    $element['#value'] = $grandTotalMax;
                    $form_state->setValueForElement($element, $grandTotalMax);
                    break;
                case $composite_name . '[possible]':
                    $element['#value'] = $grandTotalPoss;
                    $form_state->setValueForElement($element, $grandTotalPoss);
                    break;
                case $composite_name . '[actual]':
                    $element['#value'] = $grandTotalActual;
                    $form_state->setValueForElement($element, $grandTotalActual);
                    break;
                case $composite_name . '[percentage]':
                    if ($grandTotalPoss != 0) {
                        $element['#value'] =  round(($grandTotalActual / $grandTotalPoss) * 100, 2);
                        $form_state->setValueForElement(
                            $element,
                            round(($grandTotalActual / $grandTotalPoss) * 100, 2)
                            );
                    }
                    break;
            }
        }

        $element['#states']['disabled'] = [
        ];

        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';

        return $element;
    }

}
