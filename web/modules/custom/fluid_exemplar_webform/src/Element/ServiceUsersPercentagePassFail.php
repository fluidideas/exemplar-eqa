<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'service_users_percentage_pass_fail'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("service_users_percentage_pass_fail")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\ServiceUsersPercentagePassFail
 */
class ServiceUsersPercentagePassFail extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'service_users_percentage_pass_fail'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $serviceUsers = SELF::getServiceUsers();

        /**
         * homes are included in the count($serviceUsers) at the top level, we need to remove this to get an 
         * accurate count of Users -> count($serviceUsers, 1) - count($serviceUsers)
         */
        $serviceUsersCount = round(
            ($element['#percentage_amount'] / 100) * (count($serviceUsers, 1) - count($serviceUsers))
        );

        $elements['#service_user_count'] = $serviceUsersCount;

        $elements = [];
        $elements['section'] = [
            '#type' => 'item',
            '#title' => t('Section'),
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['source'] = [
            '#type' => 'item',
            '#title' => t('Source'),
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['notApplicable'] = [
            '#type' => 'checkbox',
            '#title' => 'N/A',
            '#attributes' => [
                'class' => [
                    'checkbox_na',
                ],
            ],
            // Use #after_build to add #states.
        ];

        $elements['overallPassFail'] = [
            '#type' => 'radios',
            '#title' => t('Pass/Fail'),
            '#options' => array(
                'pass' => t('Pass'),
                'fail' => t('Fail'),
            ),
            '#options_display' => 'two_columns',
            // Use #after_build to add #states.
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        for ($sampleUsers = 1; $sampleUsers <= $serviceUsersCount; $sampleUsers++) {

            $elements['sample_' . $sampleUsers . '_initials'] = [
                '#type' => 'select',
                '#title' => t('Service Users'),
                '#options' => $serviceUsers,
                '#after_build' => [[get_called_class(), 'afterBuild']],
            ];
            //kint($serviceUsers);

            $elements['sample_' . $sampleUsers . '_pass_fail'] = [
                '#type' => 'radios',
                '#title' => t(''),
                '#options' => array(
                    'pass' => t('Pass'),
                    'fail' => t('Fail'),
                ),
                '#options_display' => 'two_columns',
                '#after_build' => [[get_called_class(), 'afterBuild']],
            ];
        }

        $elements['comments'] = [
            '#type' => 'textarea',
            '#title' => t('Comments'),
            // Use #after_build to add #states.
        ];
        return $elements;
    }

    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        //Checks to see if the Element is required and disables one of the
        //radio options 'NA' so it has to be completed on the form
        if ($element['#required'] == true && $element['notApplicable']['#access'] != false) {
            $element['notApplicable']['#access'] = false;
        }
        return $element;
    }

    public static function getServiceUsers()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $taxonomyManager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $homeTid = $user->field_home->getValue();
        $home = $taxonomyManager->load($homeTid[0]['target_id']);
        $homeName = ($home != NULL) ? $home->name->getValue() : null;

        if ($homeName != null) {
            $HomeID = $taxonomyManager->loadByProperties(['name' => $homeName[0]['value'], 'vid' => 'service_users']);
            $HomeID = reset($HomeID);
            if (reset($HomeID) == null) {
                return [];
            }
            $units = $taxonomyManager->loadTree('service_users', $HomeID->id(), 2, true);
        } else {
            $units = $taxonomyManager->loadTree('service_users', 0, NULL, false);
        }
        $serviceUsers = [];
        foreach ($units as $unit) {
            // We get 1st and 2nd levels so we check parents cause only 2nd level has parents.
            if (!empty($taxonomyManager->loadParents($unit->id())) && $unit->depth != 0) {
                $serviceUsers[reset($taxonomyManager->loadParents($unit->id()))->getName()][$unit->getName()] = $unit->getName();
            }
        }
        return $serviceUsers;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];
        //kint($element);
        $element['#states']['disabled'] = [
            [':input[name="' . $composite_name . '[notApplicable]"]' => ['checked' => true]],
        ];
        // Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
        // disabling the entire table row when this element is disabled.
        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public static function validateWebformComposite(&$element, FormStateInterface $form_state, &$complete_form)
    {
        // IMPORTANT: Must get values from the $form_states since sub-elements
        // may call $form_state->setValueForElement() via their validation hook.
        // @see \Drupal\webform\Element\WebformEmailConfirm::validateWebformEmailConfirm
        // @see \Drupal\webform\Element\WebformOtherBase::validateWebformOther
        $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);

        // Only validate composite elements that are visible.
        $has_access = (!isset($element['#access']) || $element['#access'] === true);
        $is_element_required = (isset($element['#required']) && $element['#required'] == true) ? true : false;
        $is_NA = (!empty($element['notApplicable']) && $element['notApplicable']['#value'] == true) ? true : false;

        $ignored_fields = [
            'section',
            'source',
            'notApplicable',
            'sample_1_initials',
            'sample_2_initials',
            'sample_3_initials',
            'sample_4_initials',
            'sample_5_initials',
            'sample_1_pass_fail',
            'sample_2_pass_fail',
            'sample_3_pass_fail',
            'sample_4_pass_fail',
            'sample_5_pass_fail',
            'comments',
        ];

        if ($has_access) {
            // Validate required composite elements.
            $composite_elements = static::getCompositeElements($element);
            $composite_elements = WebformElementHelper::getFlattened($composite_elements);
            if ($is_element_required == true || $is_NA == false) {
                foreach ($composite_elements as $composite_key => $composite_element) {
                    if (!in_array($composite_key, $ignored_fields)) {
                        $is_empty = (isset($value[$composite_key]) && $value[$composite_key] === '');
                        if ($is_empty) {
                            $form_state->setError($element, 'Please fill out any missed data from the form');
                            //WebformElementHelper::setRequiredError($element, $form_state);
                            $element['#attributes']['class'] = ['eqa--required'];
                        }
                    }
                }
            } elseif ($is_NA == true) {
                $is_empty = (isset($value['comments']) && $value['comments'] === '');
                if ($is_empty) {
                    $form_state->setError($element, 'Please fill out any missed data from the form');
                    //WebformElementHelper::setRequiredError($element, $form_state);
                    $element['#attributes']['class'] = ['eqa--required'];
                }
            }
        }

        // Clear empty composites value.
        if (empty(array_filter($value))) {
            $element['#value'] = null;
            $form_state->setValueForElement($element, null);
        }
    }
}
