<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a 'hs_strategy_inspections'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("hs_strategy_inspections")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\HsStrategyInspections
 */
class HsStrategyInspections extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'hs_strategy_inspections'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        $elements = [];
        $elements['audit_section'] = [
            '#type' => 'textfield',
            '#title' => t('Area/s for action'),
            '#attributes' => [
                'class' => [],
            ],
        ];

        $elements['initial_score'] = [
            '#type' => 'number',
            '#title' => t('Initial Score'),
            '#attributes' => [
                'class' => [],
            ],
        ];

        $elements['review_score'] = [
            '#type' => 'number',
            '#title' => t('Review Score'),
            '#attributes' => [
                'class' => [],
            ],
        ];

        $elements['action_required'] = [
            '#type' => 'radios',
            '#title' => t('Corrective Action Required'),
            '#options' => [
                'pass' => t('Yes'),
                'fail' => t('No'),
            ],
            '#options_display' => 'two_columns',
        ];

        return $elements;
    }

}
