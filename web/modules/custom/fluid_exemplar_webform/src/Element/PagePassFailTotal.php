<?php

namespace Drupal\fluid_exemplar_webform\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a 'page_pass_fail_total'.
 *
 * Webform composites contain a group of sub-elements.
 *
 *
 * IMPORTANT:
 * Webform composite can not contain multiple value elements (i.e. checkboxes)
 * or composites (i.e. webform_address)
 *
 * @FormElement("page_pass_fail_total")
 *
 * @see \Drupal\webform\Element\WebformCompositeBase
 * @see \Drupal\fluid_exemplar_webform\Element\PagePassFailTotal
 */
class PagePassFailTotal extends WebformCompositeBase
{

    public static $pageSelection;
    public static $webformKey;
    public static $calculations = null;

    /**
     * {@inheritdoc}
     */
    public function getInfo()
    {
        return parent::getInfo() + ['#theme' => 'page_pass_fail_total'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getCompositeElements(array $element)
    {
        static::$pageSelection = $element['#page_select'];
        static::$webformKey = $element['#webform_key'];
        $elements = [];
        $elements['section'] = [
            '#type' => 'item',
            '#title' => t('Section'),
            '#description' => '',
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['maximum'] = [
            '#type' => 'textfield',
            '#title' => t('Maximum'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['possible'] = [
            '#type' => 'textfield',
            '#title' => t('Possible'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['actual'] = [
            '#type' => 'textfield',
            '#title' => t('Actual'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];
        $elements['percentage'] = [
            '#type' => 'textfield',
            '#title' => t('%'),
            '#attributes' => ['readonly' => 'readonly'],
            '#after_build' => [[get_called_class(), 'afterBuild']],
        ];

        $elements['comments'] = [
            '#type' => 'textarea',
            '#title' => t('Comments'),
        ];
        return $elements;
    }

    /**
     * Prerender function for the element
     *
     */
    public static function preRenderWebformCompositeFormElement($element)
    {
        $element = parent::preRenderWebformCompositeFormElement($element);
        return $element;
    }

    /**
     * Performs the after_build callback.
     */
    public static function afterBuild(array $element, FormStateInterface $form_state)
    {
        // Add #states targeting the specific element and table row.
        preg_match('/^(.+)\[[^]]+]$/', $element['#name'], $match);
        $composite_name = $match[1];

        static::calculateValues($element, $form_state);

        //gets all the field values from the standard and conditional pass fail elements and
        //totals up along with working out % calculations
        $url = \Drupal::service('path.current')->getPath();
        $currentPath = strpos($url, '/print/pdf/');

        if ($currentPath === false && strpos($url, '/viewSubmmission/') === false) {
            //adds field values to the element
            switch ($element['#name']) {
                case $composite_name . '[maximum]':
                    $element['#value'] = static::$calculations['totalMax'];
                    $form_state->setValueForElement($element, static::$calculations['totalMax']);
                    break;
                case $composite_name . '[possible]':
                    $element['#value'] = static::$calculations['totalPoss'];
                    $form_state->setValueForElement($element, static::$calculations['totalPoss']);
                    break;
                case $composite_name . '[actual]':
                    $element['#value'] = static::$calculations['totalActual'];
                    $form_state->setValueForElement($element, static::$calculations['totalActual']);
                    break;
                case $composite_name . '[percentage]':
                    $element['#value'] = round(static::$calculations['totalPercent'], 2);
                    $form_state->setValueForElement($element, static::$calculations['totalPercent']);
                    break;
                case $composite_name . '[section]':
                    if (!isset($element['#section'])) {
                        $sectionRemovedUnderline = str_replace('_', ' ', static::$pageSelection);
                        $removeSection = str_replace('section', '', $sectionRemovedUnderline);
                        $element['#description'] = $removeSection;
                    }

            }
        }
        /**
         *  Add .js-form-wrapper to wrapper (ie td) to prevent #states API from
         * disabling the entire table row when this element is disabled.
         */

        $element['#wrapper_attributes']['class'][] = 'js-form-wrapper';
        return $element;
    }

    public function calculateValues(array $element, $form_state)
    {
        $formFields = $form_state->getFormObject()->getWebform()->getElementsInitializedFlattenedAndHasValue();

        $totalMax = 0;
        $totalPoss = 0;
        $totalActual = 0;

        /**
         * gets the fields from the webform, loops through and looks for the specfied type
         * standard/condtional pass fail elements checks to see if the page
         * set on the total element matches the fieldset of the standard/conditional elements
         * based of the value of the fields in the elements adjusts the variables accordingly
         **/

        $componentTypesArray = [
            'standard_pass_fail',
            'conditional_pass_fail',
            'pass_fail_by',
            'service_review_pass_fail',
            'infection_control_pass_fail',
            'service_users_percentage_pass_fail'
        ];

        foreach ($formFields as $field) {
            if ($field['#webform_parent_key'] == static::$pageSelection &&
                in_array($field['#type'], $componentTypesArray)) {
                $elementValue = $form_state->getValue($field['#webform_key']);
                $totalMax++;
                if ($elementValue['notApplicable'] == "") {
                    $totalPoss++;
                    $totalActual = (
                        $elementValue['overallPassFail'] != "" &&
                        $elementValue['overallPassFail'] == 'pass'
                    ) ? $totalActual + 1 : $totalActual + 0;
                }
            }
        }

        /**
         * works out % of the section based on how many of the questions
         * were answered pass / the total poss answer answered x 100
         */

        static::$calculations = [
            'totalMax' => $totalMax,
            'totalPoss' => $totalPoss,
            'totalActual' => $totalActual,
            'totalPercent' => ($totalPoss != 0) ? ($totalActual / $totalPoss) * 100 : 0,
        ];
        

    }

}
