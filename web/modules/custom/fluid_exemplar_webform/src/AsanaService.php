<?php

namespace Drupal\fluid_exemplar_webform;

use Asana\Client;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class AsanaService.
 */
class AsanaService
{
    /**
     * The config factory.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * The asana client.
     *
     * @var \Asana\Client
     */
    protected $client;

    /**
     * Constructs a new Asana object.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The configuration factory.
     */

    /**
     * Constructs a new AsanaService object.
     */
    public function __construct(ConfigFactoryInterface $config_factory)
    {
        $this->configFactory = $config_factory;

        // Getting the personal access token.
        $personal_access_token = $this->configFactory->get('fluid_exemplar_webform.asanasettings')->get('personal_access_token');
        try {
            // Authentication between Asana and EQA using Personal Acess Token.
            $this->client = Client::accessToken($personal_access_token);
        } catch (Exception $e) {
            $a = 1;
        }
    }

    /**
     * gets a list of all the Homes in Asana
     */

    public function getAllProjects()
    {
        // Searching all the projects.
        $projects = [];
        // Getting all the workspaces.
        $workspaces = $this->client->workspaces->findAll();
        // Iterating over all the workspaces.
        foreach ($workspaces as $workspace) {
            // Getting all the projects in a workspace.
            $workspace_projects = $this->client->projects->findByWorkspace($workspace->gid);
            // Iteraring over all the projects in a workspace.
            foreach ($workspace_projects as $project) {
                $projects[$project->gid] = $project->name;
            }
        }
        return $projects;
    }

    /**
     * Gets the Project ID from Asana that matches the home from the EQA platform
     * All the homes projects on Asana have 'IHAP - ' befor the home name
     */
    public function getProjectIdByName($name)
    {
        $projects = $this->getAllProjects();
        return array_search('IHAP - ' . $name, $projects);
    }

    /**
     * Gets the User ID number from Asana that matches the Email from EQA platform
     */
    public function getUserId($email)
    {
        // Searching all the projects.
        $projects = [];
        // Getting all the workspaces.
        $workspaces = $this->client->workspaces->findAll();
        // Iterating over all the workspaces.
        foreach ($workspaces as $workspace) {
            // Getting all the projects in a workspace.
            $workspace_users = $this->client->users->findByWorkspace($workspace->gid, ['opt_fields' => 'email']);
            // Iteraring over all the projects in a workspace.
            foreach ($workspace_users as $user) {
                if (strtolower($user->email) == strtolower($email)) {
                    return $user->gid;
                }
            }
        }

    }
    

    // public function getTasks(){
    //     kint($this->getProjectIdByName('Bridgewood Mews'));
    //     $result = $this->client->tasks->getTasksForProject($this->getProjectIdByName('Test'));
    //     kint($this->client->tasks->getTask(1201338139946519));
    //     kint($result);
    // }

    /**
     * Creates a single task for a home
     */
    public function createProjectTask($projectName = '', $userName = '', $taskDesription = '')
    {
        $result = $this->client->tasks->createTask(
            [
                'name' => 'EQA Asana Action',
                "projects" => strval($this->getProjectIdByName($projectName)),
                "assignee" => $this->getUserId($userName),
                //custom fields ids will need changing to EQA
                'custom_fields' => [
                    '572719418246057' => '1189872874363028',
                    '1182363998633151' => '1182363998633168',
                ],
                'notes' => $taskDesription,
            ],
            [
                'opt_pretty' => 'true',
            ]);

    }

    /**
     * Creates Multiple Tasks for Homes in batches of 10
     */
    public function createBatchProjectTask($projectName = '', $userName = '', $tasksData = [], $webform = '' , $submittedBy = '')
    {
        $projects = strval($this->getProjectIdByName($projectName));
        $assignee = $this->getUserId($userName);
        $data = [];
        $batchCount = 0;
        $date = date("d/m/Y");
        $name = $webform . ' - ' . $submittedBy . ' - ' . $date;

        foreach ($tasksData as $task) {
            $data['actions'][] =
                [
                'method' => "POST",
                'relative_path' => '/tasks',
                'data' => [
                    "projects" => $projects,
                    'assignee' => $assignee,
                    'name' => $name,
                    //The numbers below are custom field id and custom field values id thats in asana
                    //when using drop downs
                    'custom_fields' => [
                        '572719418246057' => '1189872874363028',
                        '1182363998633151' => '1182363998633168',
                    ],
                    'notes' => $task,
                ],
            ];
            $batchCount++;
            if ($batchCount == 10) {
                $this->client->post('/batch', $data, ['opt_pretty' => 'true']);
                $data = [];
                $batchCount = 0;
            }
        }

        if(count($data) > 0 && $batchCount < 10){
            $this->client->post('/batch', $data, ['opt_pretty' => 'true']);
        }
    }

}
