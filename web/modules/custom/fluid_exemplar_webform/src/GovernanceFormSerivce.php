<?php

namespace Drupal\fluid_exemplar_webform;

/**
 * Defines a service provider for the fluid_exemplar_webfrom module.
 */
class GovernanceFormSerivce {

      /**
     * Constructs a new TakeTwentyService object.
     */
    public function __construct()
    {

    }

    public function getUserHouseName()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $homeTid = $user->field_home->getValue();
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
        return $term->name->value;
    }

    public function getFormSubmissionsData($webform_id)
    {
        $home_name = SELF::getUserHouseName();

        $webform_submissions = \Drupal::database()->select('webform_submission', 'ws');
        $webform_submissions->join('webform_submission_data', 'wsd', 'ws.sid = wsd.sid');
        $webform_submissions->condition('ws.webform_id', $webform_id, '=');
        $webform_submissions->condition('wsd.property', 'house_name', '=');
        $webform_submissions->condition('wsd.value', $home_name, '=')
            ->fields('wsd', ['sid', 'value'])
            ->fields('ws', ['completed']);
        $webform_submissions->orderBy('ws.completed', 'DESC');
        $webform_submissions = $webform_submissions->execute()->fetchAll();

        $result = (empty($webform_submissions[0]))? '0' : $webform_submissions[0];
        // kint($result);
        // die;
        return $result;

    }
}
