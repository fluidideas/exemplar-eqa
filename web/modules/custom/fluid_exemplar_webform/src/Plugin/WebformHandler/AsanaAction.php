<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform validate handler.
 *
 * @WebformHandler(
 *   id = "fluid_exemplar_webform_asana_action",
 *   label = @Translation("Asana Action Intergration"),
 *   category = @Translation("Settings"),
 *   description = @Translation("Sends data to asana for actions required. Requires Element Asana Action to be on form to work"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class AsanaAction extends WebformHandlerBase
{

    use StringTranslationTrait;

    /**
     * The token manager.
     *
     * @var \Drupal\webform\WebformTokenManagerInterface
     */
    protected $tokenManager;

    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
        $instance->tokenManager = $container->get('webform.token_manager');
        return $instance;
    }

    //default configuration values

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [];
    }

    //config form for the handler, works like any standard drupal form.

    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {

        return $this->setSettingsParents($form);
    }

    //sets the config values based on the values entered into the form
    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);
    }

    //adds validation to the webforms it self.
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        if (!$webform_submission->isDraft() && $form_state->isSubmitted()) {
            $asana = \Drupal::Service('fluid_exemplar_webform.asana');
            $tasksData = [];
            $fromValues = $form_state->getValues();
            $home = $fromValues['confirmation_box']['house_name'];
            $submit_handlers = $form_state->getSubmitHandlers();
            $webform_name = str_replace('_', ' ', $form['#webform_id']);
            /**
             * gets all the values from any asana_action element found in the form
             */
            if (!in_array("::save", $submit_handlers) && in_array("::submit", $submit_handlers)) {
                $review_access = false; 
                foreach ($form['elements']['confirmation'] as $elements) {
                    if (isset($elements['#type']) && $elements['#type'] == 'fieldset') {
                        foreach ($elements as $element) {
                            if (isset($element['#webform_plugin_id']) && $element['#webform_plugin_id'] == 'audit_review') {
                                $review_access = $element['#access'];
                                if (!empty($webform_submission->getElementData($element['#webform_key'])['review_asana_action'])) {
                                    $tasksData[] = $webform_submission->getElementData($element['#webform_key'])['review_asana_action'];
                                };
                            }
                        }
                    }
                }
                if (!$review_access) {
                    foreach ($form['elements']['audit'] as $elements) {
                        if (isset($elements['#type']) && $elements['#type'] == 'fieldset') {
                            foreach ($elements as $element) {
                                if (isset($element['#webform_plugin_id']) && $element['#webform_plugin_id'] == 'asana_action') {
                                    if (!empty($webform_submission->getElementData($element['#webform_key'])['comments'])) {
                                        $tasksData[] = $webform_submission->getElementData($element['#webform_key'])['comments'];
                                    };
                                }
                            }
                        }
                    }
                }
            }

            $current_user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $current_user_name = (!empty($current_user->field_name->getValue())) ? $current_user->field_name->getValue() : $current_user->getDisplayName();

            $user = \Drupal::entityTypeManager()->getStorage('user');
            /**
             * Gets the home manager of the home. if field_original_home is empty
             * search for home in the home_field.
             * */
            $homeManager = $user->getQuery()
                ->condition('field_original_home.entity:taxonomy_term.name', $home, '=')
                ->condition('roles', 'home_manager', '=')
                ->condition('status', '1', '=')
                ->execute();
            if (empty($homeManager)) {
                $homeManager = $user->getQuery()
                    ->condition('field_home.entity:taxonomy_term.name', $home, '=')
                    ->condition('roles', 'home_manager', '=')
                    ->condition('status', '1', '=')
                    ->execute();
            }
            $homeManagerName = (!empty($user = $user->load(reset($homeManager)))) ? $user->mail->value : '';
            if (!empty($tasksData)) {
               $asana->createBatchProjectTask($home, $homeManagerName, $tasksData, $webform_name, $current_user_name[0]['value']);
            }
        }
    }
}