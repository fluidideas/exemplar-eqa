<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformHandler;

use DateTime;
use DateTimeZone;
use Drupal\Core\Render\Markup;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Utility\Html;
use Drupal\webform\WebformInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformFormHelper;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform validate handler.
 *
 * @WebformHandler(
 *   id = "fluid_exemplar_webform_clinical_gov_validation",
 *   label = @Translation("Clinical Governance form Validation"),
 *   category = @Translation("Settings"),
 *   description = @Translation("Validate the Clinical Governance Form"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class ClinicalGovValidation extends WebformHandlerBase
{

    use StringTranslationTrait;

    /**
     * The token manager.
     *
     * @var \Drupal\webform\WebformTokenManagerInterface
     */
    protected $tokenManager;

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
        $instance->tokenManager = $container->get('webform.token_manager');
        return $instance;
    }

    //default configuration values

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration()
    {
        return [];
    }

    //config form for the handler, works like any standard drupal form.
    
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        return $this->setSettingsParents($form);
    }

    //sets the config values based on the values entered into the form
    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitConfigurationForm($form, $form_state);
    }

    //adds validation to the webforms it self.
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        $errors = $form_state->getErrors();
        foreach($errors as $element => $error){
            $elements = &WebformFormHelper::flattenElements($form['elements']);
            $elements[$element]['#wrapper_attributes']['class'][] .= 'eqa--required';
        }
    }
    
}
