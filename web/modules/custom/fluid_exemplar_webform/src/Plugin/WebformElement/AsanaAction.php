<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'asana_action' element.
 *
 * @WebformElement(
 *   id = "asana_action",
 *   label = @Translation("Asana intergration"),
 *   description = @Translation("Sends Data to Asana, needs asana handler attached to form to work"),
 *   category = @Translation("Exemplar Elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\AsanaAction
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class AsanaAction extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $lines = [];
    $lines[] = ($value['section'] ? $value['section'] : '') .
      ($value['source'] ? ' ' . $value['source'] : '') .
      ($value['overallPassFail'] ? ' Pass/Fail: ' . $value['overallPassFail'] : '') .
      ($value['comments'] ? ' ' . $value['comments']  : '');
    return $lines;
  }

}
