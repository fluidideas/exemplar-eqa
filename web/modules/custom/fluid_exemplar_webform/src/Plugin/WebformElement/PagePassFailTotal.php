<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'page_pass_fail_total' element.
 *
 * @WebformElement(
 *   id = "page_pass_fail_total",
 *   label = @Translation("Page Pass/Fail Total Element"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Exemplar Form Summary Elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\PagePassFailTotal
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class PagePassFailTotal extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $lines = [];
    $lines[] = 
      ($value['section'] ? ' ' . $value['section'] : '') .
      ($value['maximum'] ? ' Maximum: ' . $value['maximum'] : ' Maximum: ' . 0 . ' ') .
      ($value['possible'] ? ' Possible: ' . $value['possible']  : ' Possible: ' . 0 . ' ') .
      ($value['actual'] ? ' Actual: ' . $value['actual'] : ' Actual: ' . 0 . ' ') .
      ($value['percentage'] ? ' Percentage: ' . $value['percentage'] : ' Percentage: ' . 0 . ' ') .
      ($value['comments'] ? ' Comments: ' . $value['comments'] : ' Comments: ');
    return $lines;
  }

}
