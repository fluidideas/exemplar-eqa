<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement\ClinicalGovernanceElements;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'dietary_requirements_element' element.
 *
 * @WebformElement(
 *   id = "dietary_requirements_element",
 *   label = @Translation("Dietary Requirements Element"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Exemplar Clinical Governance"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements\DietaryRequirementsElement
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class DietaryRequirementsElement extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $lines = [];
    $lines[] = ($value['section'] ? $value['section'] : '') .
      ($value['simple_confirm'] ? ' confirm: ' . $value['simple_confirm'] : '');
    return $lines;
  }

}
