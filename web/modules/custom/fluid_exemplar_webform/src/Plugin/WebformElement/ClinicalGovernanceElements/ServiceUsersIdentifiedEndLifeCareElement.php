<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement\ClinicalGovernanceElements;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'service_iusers_identified_end_life_care_element' element.
 *
 * @WebformElement(
 *   id = "service_iusers_identified_end_life_care_element",
 *   label = @Translation("Service Users Identified End Life CareElement"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Exemplar Clinical Governance"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\ClinicalGovernanceElements\ServiceUsersIdentifiedEndLifeCareElement
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class ServiceUsersIdentifiedEndLifeCareElement extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $lines = [];
    $lines[] = ($value['section'] ? $value['section'] : '') .
      ($value['simple_confirm'] ? ' confirm: ' . $value['simple_confirm'] : '');
    return $lines;
  }

}
