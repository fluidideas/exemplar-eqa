<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 * Provides a 'service_users_percentage_pass_fail' element.
 *
 * @WebformElement(
 *   id = "service_users_percentage_pass_fail",
 *   label = @Translation("Conditional Pass Fail with Percentage based Service Users"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Exemplar Elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\ServiceUsersPercentagePassFail
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class ServiceUsersPercentagePassFail extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    protected function defineDefaultProperties()
    {
        // Here you define your webform element's default properties,
        // which can be inherited.
        //
        // @see \Drupal\webform\Plugin\WebformElementBase::defaultProperties
        // @see \Drupal\webform\Plugin\WebformElementBase::defaultBaseProperties
        return [
            'percentage_amount' => '',
            'service_user_count' => 0,
        ] + parent::defineDefaultProperties();
    }

    /* ************************************************************************ */

    /**
     * {@inheritdoc}
     */
    public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL)
    {
        parent::prepare($element, $webform_submission);

        // Here you can customize the webform element's properties.
        // You can also customize the form/render element's properties via the
        // FormElement.
        //
        // @see \Drupal\webform_example_element\Element\WebformExampleElement::processWebformElementExample
    }

    public function form(array $form, FormStateInterface $form_state)
    {
        $form = parent::form($form, $form_state);
        // kint($form);
        // die;
        $form['custom_properties'] = [
            '#type' => 'details',
            '#title' => t('Percentage of Service Users'),
            '#description' => t(''),
            '#open' => true,
            // Add custom properties after all fieldset elements, which have a
            // weight of -20.
            // @see \Drupal\webform\Plugin\WebformElementBase::buildConfigurationForm
            '#weight' => -10,
        ];

        $form['custom_properties']['percentage_amount'] = [
            '#type' => 'number',
            '#title' => t('Percentage of Service User'),
            '#required' => true,
            '#min' => 0,
            '#max' => 100,
            '#step' => 1,
            '#description' => t("The custom data value will be added to @label (@type) data-* attributes."),
        ];
        // Here you can define and alter a webform element's properties UI.
        // Form element property visibility and default values are defined via
        // ::defaultProperties.
        //
        // @see \Drupal\webform\Plugin\WebformElementBase::form
        // @see \Drupal\webform\Plugin\WebformElement\TextBase::form
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = [])
    {
        return $this->formatTextItemValue($element, $webform_submission, $options);
    }

    /**
     * {@inheritdoc}
     */
    protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = [])
    {
        $value = $this->getValue($element, $webform_submission, $options);
        $sample = [];
        $lines = [];
        if ($value['notApplicable'] && $value['notApplicable'] != true) {

            $lines[] = ($value['section'] ? $value['section'] : '') .
                ($value['source'] ? ' ' . $value['source'] : '') .
                ($value['overallPassFail'] ? ' ' . $value['overallPassFail'] : '');

            for ($sampleUsers = 1; $sampleUsers <= 5; $sampleUsers++) {
                $lines[] =
                    ($value['sample_' . $sampleUsers . '_initials'] ? ' ' . $value['sample_' . $sampleUsers . '_initials'] : '') .
                    ($value['sample_' . $sampleUsers . '_pass_fail'] ? ' ' . $value['sample_' . $sampleUsers . '_pass_fail'] : '');
            }

            $lines[] =
                ($sample ? ' ' . $sample : '') .
                ($value['comments'] ? ' ' . $value['comments'] : '');
        } else {
            $lines[] =
                ($value['notApplicable'] ? ' ' . 'Unable to Answer' : '') .
                ($value['comments'] ? ' Reason: ' . $value['comments'] : '');
        }
        return $lines;
    }
}
