<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'pass_fail_by' element.
 *
 * @WebformElement(
 *   id = "pass_fail_by",
 *   label = @Translation("Pass Fail By"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Exemplar Elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\PassFailBy
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class PassFailBy extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $lines = [];
    $lines[] = ($value['section'] ? $value['section'] : '') .
      ($value['source'] ? ' ' . $value['source'] : '') .
      ($value['overallPassFail'] ? ' Pass/Fail: ' . $value['overallPassFail'] : '') .
      ($value['comments'] ? ' ' . $value['comments']  : '');
    return $lines;
  }

}
