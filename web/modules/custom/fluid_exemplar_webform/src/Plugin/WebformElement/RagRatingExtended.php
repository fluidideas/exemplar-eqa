<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'rag_rating_extended' element.
 *
 * @WebformElement(
 *   id = "rag_rating_extended",
 *   label = @Translation("RAG Rating Extended"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Exemplar Form Summary Elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\RagRatingExtended
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class RagRatingExtended extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    $lines = [];
    $lines[] = 
      ($value['section'] ? ' ' . $value['section'] : '') .
      ($value['rag_green'] ? ' ' . $value['rag_green'] : 0) .
      ($value['rag_amber'] ? ' ' . $value['rag_amber']  : 0) .
      ($value['rag_red'] ? ' ' . $value['rag_red'] : 0);
    return $lines;
  }

}
