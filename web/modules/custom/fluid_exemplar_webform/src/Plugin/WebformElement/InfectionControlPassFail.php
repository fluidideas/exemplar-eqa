<?php

namespace Drupal\fluid_exemplar_webform\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'infection_control_pass_fail' element.
 *
 * @WebformElement(
 *   id = "infection_control_pass_fail",
 *   label = @Translation("Infection Control Multi Pass Fail"),
 *   description = @Translation("Provides a webform element example."),
 *   category = @Translation("Exemplar Elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 *
 * @see \Drupal\fluid_exemplar_webform\Element\InfectionControlPassFail
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class InfectionControlPassFail extends WebformCompositeBase
{

    /**
     * {@inheritdoc}
     */
    protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = [])
    {
        return $this->formatTextItemValue($element, $webform_submission, $options);
    }

    /**
     * {@inheritdoc}
     */
    protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = [])
    {
        $value = $this->getValue($element, $webform_submission, $options);
        $sample = [];
        $lines = [];
        if ($value['notApplicable'] && $value['notApplicable'] != true) {
            
            $lines[] = ($value['section'] ? $value['section'] : '') .
                ($value['source'] ? ' ' . $value['source'] : '') .
                ($value['overallPassFail'] ? ' ' . $value['overallPassFail'] : '');

            for ($sampleUsers = 1; $sampleUsers <= 5; $sampleUsers++) {
                $lines[] =
                    ($value['sample_' . $sampleUsers . '_initials'] ? ' ' . $value['sample_' . $sampleUsers . '_initials'] : '') .
                    ($value['sample_' . $sampleUsers . '_pass_fail'] ? ' ' . $value['sample_' . $sampleUsers . '_pass_fail'] : '');
            }

            $lines[] =
                ($sample ? ' ' . $sample : '') .
                ($value['comments'] ? ' ' . $value['comments'] : '');
        }else{
          $lines[] = 
            ($value['notApplicable'] ? ' ' . 'Unable to Answer' : '') .
            ($value['comments'] ? ' Reason: ' . $value['comments'] : '');
        }
        return $lines;
    }

}
