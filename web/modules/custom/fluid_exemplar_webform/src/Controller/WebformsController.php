<?php

namespace Drupal\fluid_exemplar_webform\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebformsController.
 */
class WebformsController extends ControllerBase
{

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        $instance = parent::create($container);
        return $instance;
    }

    public function __construct()
    {
        //$this->taxonomy = $fluidExemplarReportingPopulateHomeTaxonomy;
    }

    /**
     * Generatedashboard.
     *
     * @return string
     *   Return Hello string.
     */
    public function clinicalGovernanceSubmissionDuplication()
    {

        $response = new RedirectResponse("/form/weekly-clinical-governance");

        $formsService = \Drupal::Service('fluid_exemplar_webform.governance_form_serivce');
        $governanceForm = $formsService->getFormSubmissionsData('weekly_clinical_governance');
        $governanceFormData = \Drupal\webform\Entity\WebformSubmission::load($governanceForm->sid);
        
        if ($governanceFormData) {
            $data = $governanceFormData->createDuplicate();
            $data->set('in_draft', TRUE);
            $data->save();

            $response = new RedirectResponse("/form/weekly-clinical-governance?token=" . $data->getToken());
        }

        $response->send();
    }
}
