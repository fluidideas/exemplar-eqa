<?php

namespace Drupal\Tests\webform_example_composite\Functional;

use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\Tests\webform\Functional\WebformBrowserTestBase;

/**
 * Tests for webform example composite.
 *
 * @group webform_example_composite
 */
class WebformExampleCompositeTest extends WebformBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['fluid_exemplar_webform'];

  /**
   * Tests webform example element.
   */
  public function testWebformExampleComposite() {
    $webform = Webform::load('standard_pass_fail');

    // Check form element rendering.
    $this->drupalGet('/webform/standard_pass_fail');
    // NOTE:
    // This is a very lazy but easy way to check that the element is rendering
    // as expected.
    $this->assertRaw('<label for="edit-standard-pass-fail-first-name">First name</label>');
    $this->assertFieldById('edit-standard-pass-fail-first-name');
    $this->assertRaw('<label for="edit-standard-pass-fail-last-name">Last name</label>');
    $this->assertFieldById('edit-standard-pass-fail-last-name');
    $this->assertRaw('<label for="edit-standard-pass-fail-date-of-birth">Date of birth</label>');
    $this->assertFieldById('edit-standard-pass-fail-date-of-birth');
    $this->assertRaw('<label for="edit-standard-pass-fail-gender">Gender</label>');
    $this->assertFieldById('edit-standard-pass-fail-gender');

    // Check webform element submission.
    $edit = [
      'standard_pass_fail[first_name]' => 'John',
      'standard_pass_fail[last_name]' => 'Smith',
      'standard_pass_fail[gender]' => 'Male',
      'standard_pass_fail[date_of_birth]' => '1910-01-01',
      'standard_pass_fail_multiple[items][0][first_name]' => 'Jane',
      'standard_pass_fail_multiple[items][0][last_name]' => 'Doe',
      'standard_pass_fail_multiple[items][0][gender]' => 'Female',
      'standard_pass_fail_multiple[items][0][date_of_birth]' => '1920-12-01',
    ];
    $sid = $this->postSubmission($webform, $edit);
    $webform_submission = WebformSubmission::load($sid);
    $this->assertEqual($webform_submission->getElementData('standard_pass_fail'), [
      'first_name' => 'John',
      'last_name' => 'Smith',
      'gender' => 'Male',
      'date_of_birth' => '1910-01-01',
    ]);
    $this->assertEqual($webform_submission->getElementData('standard_pass_fail_multiple'), [
      [
        'first_name' => 'Jane',
        'last_name' => 'Doe',
        'gender' => 'Female',
        'date_of_birth' => '1920-12-01',
      ],
    ]);
  }

}
