<?php
/**
 * Implements hook_views_data().
 */
function fluid_exemplar_reporting_views_data()
{

    $data['webform_submission_data'] = [
        'table' => [
            'group' => t('Fluid Exemplar Reporting Module'),
            'provider' => 'webform_submission_data',
            'base' => [
                'field' => 'sid',
                'title' => t('Webform Submission Data'),
                'help' => t('This table contains datas related Webform Submissions'),
            ],
            'join' => [
                'webform_submission' => [
                    'left_field' => 'sid',
                    'field' => 'sid',
                ],
            ],
        ],
        // Defines custom table to views.
        'webform_id' => [
            'title' => t('Webform ID'),
            'help' => t('The unique ID.'),
            'field' => [
                'id' => 'string',
            ],
            'filter' => [
                'id' => 'string',
            ],
            'sort' => [
                'id' => 'standard',
            ],
            'argument' => [
                'id' => 'numeric',
            ],
        ],
        'sid' => [
            'title' => t('SID'),
            'help' => t('Submission ID'),
            'field' => [
                'id' => 'numeric',
            ],
            'filter' => [
                'id' => 'numeric',
            ],
            'argument' => [
                'id' => 'numeric',
            ],
        ],
        'name' => [
            'title' => t('Section Id'),
            'help' => t('Section Id'),
            'field' => [
                'id' => 'standard',
            ],
            'filter' => [
                'id' => 'string',
            ],
            'argument' => [
                'id' => 'string',
            ],
        ],
        'property' => [
            'title' => t('Property'),
            'help' => t('Section field name'),
            'field' => [
                'id' => 'standard',
            ],
            'filter' => [
                'id' => 'string',
            ],
            'argument' => [
                'id' => 'string',
            ],
        ],
        'value' => [
            'title' => t('Answer'),
            'help' => t('Field Answer'),
            'field' => [
                'id' => 'standard',
            ],
            'filter' => [
                'id' => 'string',
            ],
            'argument' => [
                'id' => 'string',
            ],
        ],
        'sid' => [
            'title' => t('Submission ID'),
            'help' => t('The full user object.'),
            'relationship' => [
                'group' => t('Webform'),
                'label' => t('Submission data'),
                'title' => t('Submission Data'),
                'help' => t('Display user informations of custom table user.'),
                'base' => 'webform_submission', // database table
                'base field' => 'sid', //database field to join on
                'relationship field' => 'sid', //relationship to join with
                'id' => 'standard',
            ],
        ],
    ];

    return $data;
}
