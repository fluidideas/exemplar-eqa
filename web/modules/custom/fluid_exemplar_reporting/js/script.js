function add_weeks(date, numWeeks) {
  newDate = new Date(date.setDate(date.getDate() + (numWeeks * 7)));
  formatDateTime = newDate.toLocaleDateString('en-GB').split('.000Z')[0];
  return formatDateTime;

}

function subtract_weeks(date, numWeeks) {
  newDate = new Date(date.setDate(date.getDate() - (numWeeks * 7)));
  formatDateTime = newDate.toLocaleDateString('en-GB').split('.000Z')[0];
  return formatDateTime;

}

function add_days(date, numDays) {
  newDate = new Date(date.setDate(date.getDate() + numDays));
  formatDateTime = newDate.toLocaleDateString('en-GB').split('.000Z')[0];
  return formatDateTime;

}

function subtract_days(date, numDays) {
  newDate = new Date(date.setDate(date.getDate() - numDays));
  formatDateTime = newDate.toLocaleDateString('en-GB').split('.000Z')[0];
  return formatDateTime;

}

function formatDate(date){
    var res = date.toString().split("/");
    [res[0], res[res.length - 1]] = [res[res.length - 1], res[0]];
    var str = res.join('-')
    return str;
}


function dateChanger() {
  // Your code goes here....
  $('.previous_day').mousedown(function () {
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));

    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);

    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(subtract_days(starDate, 1));
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(subtract_days(endDate, 1));

    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });

  $('.previous_week').click(function () {
    //console.log($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val());
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));

    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);

    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(subtract_weeks(starDate, 1).toString());
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(subtract_weeks(endDate, 1).toString());

    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });

  $('.previous_period').mousedown(function () {
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));
    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);
    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(subtract_weeks(starDate, 4).toString());
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(subtract_weeks(endDate, 4).toString());

    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });

  $('.previous_3rd').mousedown(function () {
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));
    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);

    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(subtract_weeks(starDate, 12).toString());
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(subtract_weeks(endDate, 12).toString());
    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });

  $('.next_day').mousedown(function () {
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));

    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);

    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(add_days(starDate, 1).toString());
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(add_days(endDate, 1).toString());

    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });

  $('.next_week').mousedown(function () {
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));

    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);

    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(add_weeks(starDate, 1).toString());
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(add_weeks(endDate, 1).toString());
    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });

  $('.next_period').mousedown(function () {
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));

    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);

    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(add_weeks(starDate, 4).toString());
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(add_weeks(endDate, 4).toString());
    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });

  $('.next_3rd').mousedown(function () {
    starDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[min]"').val()));
    endDate = new Date(formatDate($(this).closest('.views-exposed-form').find('input[name="completed[max]"').val()));
    // starDate.setHours(0, 0, 0);
    // endDate.setHours(23, 59, 59);

    $(this).closest('.views-exposed-form').find('input[name="completed[min]"').val(add_weeks(starDate, 12).toString());
    $(this).closest('.views-exposed-form').find('input[name="completed[max]"').val(add_weeks(endDate, 12).toString());
    $(this).closest('.views-exposed-form').find(':submit').trigger('click');
  });
}

$(document).ready(function () {
  dateChanger();
});

$(document).ajaxComplete(function (event, xhr, settings) {
  dateChanger();
});// end of ajax
