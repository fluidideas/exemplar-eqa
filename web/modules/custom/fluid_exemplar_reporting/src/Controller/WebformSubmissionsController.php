<?php

namespace Drupal\fluid_exemplar_reporting\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class WebformSubmissionsController.
 */
class WebformSubmissionsController extends ControllerBase
{

    /**
     * Viewsubmmission.
     *
     * @return string
     *   Return Hello string.
     */
    public function viewSubmmission($submissionId)
    {
        //kint($submissionId);
        $submission = \Drupal\webform\Entity\WebformSubmission::load($submissionId);

        $build = \Drupal::service('entity.form_builder')->getForm($submission, 'edit');

        return $build;
    }

}
