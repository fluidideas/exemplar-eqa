<?php

namespace Drupal\fluid_exemplar_reporting\Controller;

use GuzzleHttp\Client;
use Drupal\views\Views;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DashboardController.
 */
class DashboardController extends ControllerBase
{

    /**
     * Drupal\fluid_exemplar_reporting\PopulateHomeTaxonomy definition.
     *
     * @var \Drupal\fluid_exemplar_reporting\PopulateHomeTaxonomy
     */
    protected $fluidExemplarReportingPopulateHomeTaxonomy;

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        $instance = parent::create($container);
        $instance->fluidExemplarReportingPopulateHomeTaxonomy = $container->get('fluid_exemplar_reporting.populate_home_taxonomy');
        return $instance;
    }

    public function __construct()
    {
        //$this->taxonomy = $fluidExemplarReportingPopulateHomeTaxonomy;
    }

    /**
     * Generatedashboard.
     *
     * @return string
     *   Return Hello string.
     */
    public function generateDashboard()
    {
        $renderArray;
        
        //Firstly, get the view in question.
        $view = Views::getView('audit_reporting');
        $view->setDisplay('block_1');
        $renderArray['content']['daily'] = ['view' => $view->buildRenderable()];
        
        $view = Views::getView('audit_reporting');
        $view->setDisplay('block_2');
        $renderArray['content']['weekly'] =['view' => $view->buildRenderable()];

        $view = Views::getView('audit_reporting');
        $view->setDisplay('block_3');
        $renderArray['content']['current'] = ['view' => $view->buildRenderable()];

        $view = Views::getView('audit_reporting');
        $view->setDisplay('block_4');
        $renderArray['content']['3rd_period'] = ['view' => $view->buildRenderable()];

        //kint($renderArray);

        return $renderArray;
    }

}
