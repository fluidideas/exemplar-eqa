<?php

namespace Drupal\fluid_exemplar_reporting;

use DateTime;

/**
 * Class DashboardService.
 */
class DashboardService
{

    private $webformId;
    private $dateRangeMin;
    private $dateRangeMax;
    private $userHomeName;
    private $userRoles;
    private $isHomeManager;

    /**
     * Constructs a new DashboardService object.
     */
    public function __construct()
    {

    }

    public function getUser()
    {
        return \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    }

    public function getUserRoles()
    {
      return $this->getUser()->getRoles();
    }

    public function getUserHome()
    {
        $user = $this->getUser();
        $homeTid = (!empty($user->field_home->getValue()))? $user->field_home->getValue() : $user->field_original_home->getValue() ;
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);

        return $term->name->value;
    }

    public function isHomeManager(){
      return (in_array('home_manager', $this->userRoles)) ? true : false;
    }

    public function setVariables($variables){
      $this->webformId = $variables['row']->webform_submission_webform_id;
      $this->userHomeName = $this->getUserHome();
      $this->userRoles = $this->getUserRoles();
      
      $viewFilters = $variables['view']->filter;
      $webformCompletedDates = $viewFilters['completed']->value;
        
      $this->dateRangeMin = new DateTime(str_replace("/","-",$webformCompletedDates['min']));
      $this->dateRangeMin->setTime('00', '00', '00');
      $this->dateRangeMax = new DateTime(str_replace("/","-",$webformCompletedDates['max']));
      $this->dateRangeMax->setTime('23', '59', '59');
      $this->isHomeManager = $this->isHomeManager();
    }

    public function ragCheck()
    {
        $rag_check = \Drupal::database()->select('webform_submission_data', 'wsd');
        $rag_check->condition('wsd.property', 'rag_' . '%', 'LIKE');
        $rag_check->condition('wsd.webform_id', $this->webformId, '=')
            ->fields('wsd', ['webform_id']);
        $rag_check = $rag_check->countQuery()->execute()->fetchAll();
      
        if ($rag_check[0]->expression != 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * returns an array of of submission IDs for a set home
     */
    public function getWebformIdsByHome()
    {
        $homeCheck = \Drupal::database()->select('webform_submission_data', 'wsd');
        $homeCheck->condition('wsd.property', 'house_name', 'LIKE');
        $homeCheck->condition('wsd.value', $this->userHomeName, '=')
            ->fields('wsd', ['sid']);
        $homeCheck = (empty($homeCheck->execute()->fetchCol()) ? [0] : $homeCheck->execute()->fetchCol());

        return $homeCheck;
    }

    public function getRagInfo()
    {
        //gathers the rag raiting information

        if ($this->ragCheck()) {
            $webform_submissions = \Drupal::database()->select('webform_submission', 'ws');
            $webform_submissions->Join('webform_submission_data', 'wsd', 'ws.sid = wsd.sid');

            if($this->isHomeManager){
              $webform_submissions->condition('wsd.sid', $this->getWebformIdsByHome(), 'IN');
            }

            $webform_submissions->condition('ws.completed', $this->dateRangeMin->getTimestamp(), '>=');
            $webform_submissions->condition('ws.completed', $this->dateRangeMax->getTimestamp(), '<=');
            $webform_submissions->condition('ws.webform_id', $this->webformId, '=');
            $webform_submissions->condition('wsd.property', ['rag_green', 'rag_amber', 'rag_red', 'rag_blue'], 'IN');
            $webform_submissions->condition('wsd.value', 0, '>')
                ->fields('wsd', ['sid', 'property', 'webform_id'])
                ->fields('ws', ['completed']);
            $webform_submissions->orderBy('ws.completed', 'DESC');
            $webform_submissions = $webform_submissions->execute()->fetchAll();
            $ragTotals = [
                'green' => 0,
                'amber' => 0,
                'red' => 0,
                'blue' => 0,
            ];
        } else {
            $ragTotals = [
                'green' => '',
                'amber' => '',
                'red' => '',
                'blue' => '',
            ];
        }

        //creates a count for each rag raiting.
        foreach ($webform_submissions as $webform_results) {
            if ($this->webformId == $webform_results->webform_id) {
                switch ($webform_results->property) {
                    case 'rag_green':
                        $ragTotals['green'] = $ragTotals['green'] + 1;
                        break;
                    case 'rag_amber':
                        $ragTotals['amber'] = $ragTotals['amber'] + 1;
                        break;
                    case 'rag_red':
                        $ragTotals['red'] = $ragTotals['red'] + 1;
                        break;
                    case 'rag_blue':
                        $ragTotals['blue'] = $ragTotals['blue'] + 1;
                        break;
                }
            }
        }

        return $ragTotals;
    }

    public function getOverallFormCount()
    {
        //gets the overall count of the number of forms that need to be completed
        $webform_count = \Drupal::database()->select('taxonomy_term__field_webforms_to_load', 'ttwl');
        
        if($this->isHomeManager){
          $webform_count->Join('taxonomy_term_field_data', 'ttfd', 'ttwl.entity_id = ttfd.tid');
          $webform_count->condition('ttfd.name', $this->userHomeName, '=');
        }

        $webform_count->condition('ttwl.field_webforms_to_load_target_id', $this->webformId, '=')
            ->fields('ttwl', ['field_webforms_to_load_target_id']);
        $webform_count = $webform_count->countQuery()->execute()->fetchAll();

        return $webform_count[0]->expression;
    }

    public function getTotalFormSubmissions()
    {
        //gets the count of the total webform submissions
        $webform_submissions_count = \Drupal::database()->select('webform_submission', 'ws');

        if($this->isHomeManager){
          $webform_submissions_count->condition('ws.sid', $this->getWebformIdsByHome(), 'IN');
        }

        $webform_submissions_count->condition('ws.completed', $this->dateRangeMin->getTimestamp(), '>=');
        $webform_submissions_count->condition('ws.completed', $this->dateRangeMax->getTimestamp(), '<=');
        $webform_submissions_count->condition('ws.webform_id', $this->webformId, '=')
            ->fields('ws', ['sid', 'webform_id', 'completed']);
        $webform_submissions_count = $webform_submissions_count->countQuery()->execute()->fetchAll();
        
        return $webform_submissions_count[0]->expression;
    }

    public function getCompletedFormsHomesList(){
      //gets all the completed submissions for a form along with home name
        $webform_completed = \Drupal::database()->select('webform_submission', 'ws');
        $webform_completed->Join('webform_submission_data', 'wsd', 'ws.sid = wsd.sid');

        if($this->isHomeManager){
          $webform_completed->condition('wsd.sid', $this->getWebformIdsByHome(), 'IN');
        }

        $webform_completed->condition('ws.completed', $this->dateRangeMin->getTimestamp(), '>=');
        $webform_completed->condition('ws.completed', $this->dateRangeMin->getTimestamp(), '<=');
        $webform_completed->condition('ws.webform_id', $this->webformId, '=');
        $webform_completed->condition('wsd.property', 'house_name', '=')
            ->fields('wsd', ['sid', 'property', 'webform_id', 'value'])
            ->fields('ws', ['completed']);
        $webform_completed->orderBy('ws.completed', 'DESC');
        $webform_completed = $webform_completed->execute()->fetchAll();

        //creates an array of homes that have been completed to use later on to form a list of the remaining homes
        $homesNotOutstandingStr = '';
        $homesNotOutstandingArray = [];
        foreach ($webform_completed as $completed) {
            if (!in_array($completed->value, $homesNotOutstandingArray)) {
                array_push($homesNotOutstandingArray, $completed->value);
                if (strlen($homesNotOutstandingStr) != 0) {
                    $homesNotOutstandingStr = $homesNotOutstandingStr . "," . $completed->value;
                } else {
                    $homesNotOutstandingStr = $completed->value;
                }
            }
        }

        return $homesNotOutstandingStr;
    }

    public function getDashboardData($variables){
      $this->setVariables($variables);

      $totalFromSubmissions = $this->getTotalFormSubmissions();
      $ragTotals = $this->getRagInfo();

      $data = [
        'form_count' => $totalFromSubmissions,
        'homesNotOutstanding' => $this->getCompletedFormsHomesList(),
        'total_forms' => $this->getOverallFormCount() - $totalFromSubmissions,
        'rag_green' => $ragTotals['green'],
        'rag_amber' => $ragTotals['amber'],
        'rag_red' => $ragTotals['red'],
        'rag_blue' => $ragTotals['blue'],
      ];

      return $data;
    }

}
