<?php

namespace Drupal\fluid_exemplar_reporting;

use Drupal\taxonomy\Entity\Term;

/**
 * Class PopulateHomeTaxonomy.
 */
class PopulateHomeTaxonomy
{

    /**
     * Constructs a new PopulateHomeTaxonomy object.
     */
    public function __construct()
    {
    }

    public function getHomeJsonData()
    {
        return json_decode(file_get_contents('https://exemplar.fluid-staging.co.uk/rest/care-homes'));
    }

    //still needs a cron job to be automatic
    public function generateHomeTerms()
    {
        $vocabName = 'homes';
        $homes = self::getHomeJsonData();


        foreach ($homes as $home) {
            $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
                ->loadByProperties(['name' => $home->title, 'vid' => $vocabName]);
            $term = reset($term);
            if ($term) {
                return $term->id();
            }
            //Create the taxonomy term.
            $new_term = Term::create([
                'name' => $home->title,
                'vid' => $vocabName,
                'field_image_url' => $home->image,
                'field_home_id' => SELF::getHomeId($home->title),
            ]);
            // Save the taxonomy term.
            $new_term->save();
        }
    }

    public function getHomeId($homeName)
    {
        $ApiService = \Drupal::Service('fluid_exemplar_reporting.hcapi');
        $homes = $ApiService->getHomes();
        foreach ($homes as $home) {
            if ($home->name == $homeName) {
                return $home->home_id;
            }
        }
        return null;
    }

    //only to be used for testing
    public function clearHomesTaxonomy()
    {
        $tids = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->getQuery()
            ->condition('vid', 'homes')
            ->accessCheck(false)
            ->execute();

        $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $entities = $controller->loadMultiple($tids);
        $controller->delete($entities);
    }
}
