<?php

namespace Drupal\fluid_exemplar_reporting\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Class AdminHomeSwitchForm.
 */
class AdminHomeSwitchForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'admin_home_switch_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $currentHomeID = SELF::getHome();

        $form['home'] = [
            '#type' => 'select',
            '#title' => $this
                ->t('Select a Home'),
            '#options' => SELF::getHomes(),
            '#default_value' => $currentHomeID[0]['target_id'],
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#title' => 'Select home',
            '#value' => $this->t('Select home'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        foreach ($form_state->getValues() as $key => $value) {
            // @TODO: Validate fields.
        }
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $home = $form_state->getValue('home');
        $user = User::load(\Drupal::currentUser()->id());
        // if (empty($user->field_original_home->target_id)) {
        //     $user->field_original_home->target_id = $user->field_home->target_id;
        // }
        $user->field_home->target_id = $home;
        $user->save();

    }

    public function getHomes()
    {

        $homesTaxon = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('homes', $parent = 0, $max_depth = 1, $load_entities = false);
        foreach ($homesTaxon as $homes) {
            $homesList[$homes->tid] = $homes->name;
        }

        $allowedHomesList = [];

        if ($this->isHomeManager()) {
            $currentHome = $this->getCurrentHome();

            $currentHomeID = $currentHome[0]['target_id'];

            $allowedHomes = $this->getAllowedHomes();
            foreach ($homesList as $key => $home) {
                if (in_array($key, $allowedHomes)) {
                    $allowedHomesList[$key] = $home;
                }
            }

            if(!in_array($currentHomeID, $allowedHomes))
            {   
                
                $allowedHomesList[$currentHomeID] = $homesList[$currentHomeID];
            }

            return $allowedHomesList;
        }

        return $homesList;
    }

    public function isHomeManager()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

        if (in_array('home_manager', $user->getRoles())) {
            return true;
        }
        return false;
    }

    public function getAllowedHomes()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $homesList = [];
        foreach ($user->get('field_allowed_homes')->getValue() as $home) {
            $homesList[] = $home['target_id'];
        }
        return $homesList;
    }

    private function getCurrentHome()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $mainHome = $user->get('field_original_home')->getValue();
        
        if($mainHome[0]['target_id'] != 0){
            return $mainHome;
        }

        return $user->get('field_home')->getValue();
    }

    private function getHome()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

        return $user->get('field_home')->getValue();
    }

}
