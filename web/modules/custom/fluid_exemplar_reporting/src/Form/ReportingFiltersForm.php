<?php

namespace Drupal\fluid_exemplar_reporting\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ReportingFiltersForm.
 */
class ReportingFiltersForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'reporting_filters_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['home'] = [
            '#type' => 'select',
            '#title' => $this
                ->t('Select Form Category'),
            '#options' => SELF::getHomes(),
        ];

        $form['periods'] = [
            '#type' => 'select',
            '#title' => $this
                ->t('Select Form Category'),
            '#options' => [
                'Daily' => $this
                    ->t('Daily'),
                'Weekly' => $this
                    ->t('Weekly'),
                'Current Period' => $this
                    ->t('Current Period'),
                'Every 3rd Period' => $this
                    ->t('Every 3rd Period'),
            ],
        ];
        $form['completed_date'] = [
            '#type' => 'date',
            '#title' => $this
                ->t('Date'),
            '#date_format' => 'd-m-Y',
        ];

        $year = range(1910, date("Y"));
        arsort($year);
        $form['year'] = [
            '#type' => 'select',
            '#title' => $this
                ->t('Select a Year'),
            '#options' => $year,
        ];

        $week = range(1, 52);
        $form['week'] = [
            '#type' => 'select',
            '#title' => $this
                ->t('Select a week'),
            '#options' => $week,
            '#states' => [
                'visible' => [
                    ':input[name="periods"]' => ['value' => 'weekly'],
                ],
            ],
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        foreach ($form_state->getValues() as $key => $value) {
            // @TODO: Validate fields.
        }
        parent::validateForm($form, $form_state);
    }

    public function getHomes()
    {
        $homesTaxon = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('homes', $parent = 0, $max_depth = 1, $load_entities = false);
        $homesList['All'] = 'All';
        foreach ($homesTaxon as $homes) {
            $homesList[$homes->name] = $homes->name;
        }
        return $homesList;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $form_state->setRedirect('fluid_exemplar_reporting.dashboard_controller_generateDashboard', [
            'periods' => $form_state->getValue('periods'),
            'home' => $form_state->getValue('home'),
            'year' => $form_state->getValue('year'),
            'week' => $form_state->getValue('week'),
            'created' => $form_state->getValue('completed_date'),
        ]
        );
    }

}

//notes
//date('Y-m-d',strtotime('2016W01')); first week of the year
