<?php

namespace Drupal\fluid_exemplar_reporting;

/**
 * Class FormsDashboardService.
 */
class FormsDashboardService
{
    protected $homeName;
    protected $config;
    protected $todaysDate;
    /**
     * Constructs a new FormsDashboardService object.
     */
    public function __construct()
    {
        $this->homeName = $this->getUserHouseName();
        $this->config = \Drupal::service('config.factory')->getEditable('fluid_exemplar_reporting.settings');
        $this->todaysDate = strtotime(date('d-m-Y'));
    }

    public function getUserHouseName()
    {
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $homeTid = (!empty($user->field_home->getValue()))? $user->field_home->getValue() : $user->field_original_home->getValue()  ;
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($homeTid[0]['target_id']);
        return $term->name->value;
    }

    public function deleteDraftSubmission($sid)
    {
        $webform_submission = \Drupal\webform\Entity\WebformSubmission::load($sid);
        // Check if submission is returned.
        if (!empty($webform_submission)) {
            //Delete the Submission
            if ($webform_submission->isDraft()) {
                $webform_submission->delete();
            }
        }
    }

    public function hasDraft($webform_id, $timePeriod)
    {  

        $webform_submissions = \Drupal::database()->select('webform_submission', 'ws');
        $webform_submissions->join('webform_submission_data', 'wsd', 'ws.sid = wsd.sid');
        $webform_submissions->condition('ws.webform_id', $webform_id, '=');
        $webform_submissions->condition('ws.in_draft', 1, '=');
        $webform_submissions->condition('wsd.property', 'house_name', '=');
        $webform_submissions->condition('wsd.value', $this->homeName, '=')
            ->fields('wsd', ['sid'])
            ->fields('ws', ['created', 'token']);
        $webform_submissions->orderBy('ws.created', 'DESC');
        $webform_submissions = $webform_submissions->execute()->fetchAll();

        $data = null;

        $latest_webform = reset($webform_submissions);
        if ($latest_webform != null) {
            $date = $latest_webform->created;
        } else {
            return false;
        }
        
        $formSubmissionDate = strtotime(date('d-m-Y', $date));
        switch ($timePeriod) {
            case 'Daily':
                if ($formSubmissionDate == $this->todaysDate) {
                    return $latest_webform;
                } else {
                    $this->deleteDraftSubmission($latest_webform->sid);
                }
            case 'Weekly':
                $currentWeek = date('W');
                $submissionWeek = date('W', $formSubmissionDate);
                if ($submissionWeek == $currentWeek) {
                    return $latest_webform;
                } else {
                    $this->deleteDraftSubmission($latest_webform->sid);
                }
                break;
            case 'Current Period':
            case 'Every 3rd Period':
                $periodDate = $this->config->get('currentPeriod');
                if ($timePeriod == 'Every 3rd Period') {
                    $periodDate = $this->config->get('current3rdPeriod');
                }

                if ($formSubmissionDate >= $periodDate['start_date'] && $formSubmissionDate <= $periodDate['end_date']) {
                    return $latest_webform;
                } else {
                    $this->deleteDraftSubmission($latest_webform->sid);
                }
                break;
            case 'Annual':
                $currentYear = date('Y');
                $submissionYear = date('Y', $formSubmissionDate);
                if($submissionYear == $currentYear){
                    return $latest_webform;
                }else {
                    $this->deleteDraftSubmission($latest_webform->sid);
                }
                break;
        }

        return false;
    }

    public function getGroupedWebforms()
    {

        $allowedWebforms = $this->getAllowedWebforms();
        $webformsConfig = \Drupal::entityTypeManager()->getStorage('webform')->loadMultiple(null);
        $this->getPeriod();
        $categories = [];
        foreach ($webformsConfig as $webformData) {
                if (!empty($webformData->get('categories')) && in_array($webformData->get('id'), $allowedWebforms)) {
                    $hasData = $this->hasDraft($webformData->get('id'), $webformData->get('categories')[0]);
                    
                    $data = [
                        'id' => $webformData->get('id'),
                        'url' => ($webformData->get('id') == 'weekly_clinical_governance' && $hasData == false) ? "/webform/clinical-governance-duplicate" : $webformData->toUrl()->toString(),
                        'title' => $webformData->get('title'),
                    ];

                    if ($hasData != false) {
                        $data['draft'] = true;
                        $data['draftUrl'] = $data['url'] . '?token=' . $hasData->token;
                    }

                    $categories[strtolower(str_replace(' ', '_', $webformData->get('categories')[0]))][] = $data;

                    if ($this->formNeedsAttention($webformData->get('id'), $webformData->get('categories')[0])) {
                        $categories['needs_attention'][] = $data;
                    }

                }

        }

        return $categories;

    }

    public function getAllowedWebforms()
    {
        $HomeTerm = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term')
            ->loadByProperties([
                'vid' => 'homes',
                'name' => $this->homeName,
            ]);

        $homeWebforms = $HomeTerm[key($HomeTerm)]->get('field_webforms_to_load')->getValue();

        $webformsList = [];

        foreach ($homeWebforms as $webforms) {
            array_push($webformsList, $webforms['target_id']);
        }
        return $webformsList;
    }

    public function getLastSubmissionDate($webform_id)
    {

        $webform_submissions = \Drupal::database()->select('webform_submission', 'ws');
        $webform_submissions->join('webform_submission_data', 'wsd', 'ws.sid = wsd.sid');
        $webform_submissions->condition('ws.webform_id', $webform_id, '=');
        $webform_submissions->condition('ws.in_draft', 0, '=');
        $webform_submissions->condition('wsd.property', 'house_name', '=');
        $webform_submissions->condition('wsd.value', $this->homeName, '=')
            ->fields('wsd', ['sid', 'value'])
            ->fields('ws', ['completed']);
        $webform_submissions->orderBy('ws.completed', 'DESC');
        $webform_submissions = $webform_submissions->execute()->fetchAll();

        $date = null;

        $latest_webform = reset($webform_submissions);
        if ($latest_webform != null) {
            $date = $latest_webform->completed;
            return $date;
        }

        return $date;
    }

    public function take20NeedsAttention()
    {

        $clinicalDaily = date('d-m-Y', $this->getLastSubmissionDate('daily_clinical_walk_round'));
        $managerDaily = date('d-m-Y', $this->getLastSubmissionDate('daily_home_manager_walk_round'));
        if (strtotime($clinicalDaily) == $this->todaysDate && strtotime($managerDaily) == $this->todaysDate) {
            return true;
        }

        return false;

    }

    public function formNeedsAttention($webformId, $timePeriod)
    {
        $formSubmissionDate = strtotime(date('d-m-Y', $this->getLastSubmissionDate($webformId)));

        if ($formSubmissionDate == null) {
            return true;
        }

        $currentTime = date('H:i:s');
        switch ($timePeriod) {
            case 'Daily':
                if ($formSubmissionDate == $this->todaysDate) {
                    return false;
                } else {
                    switch ($webformId) {
                        case 'daily_clinical_walk_round':
                        case 'daily_home_manager_walk_round':
                            if ($currentTime >= date('H:i:s', strtotime('today 9am'))) {
                                return true;
                            }
                            return false;
                            break;
                        case 'take20':
                            return $this->take20NeedsAttention();
                            break;
                    }
                }
            case 'Weekly':
                $currentWeek = date('W');
                $submissionWeek = date('W', $formSubmissionDate);
                $startReferenceDate = (date('D', $this->todaysDate) === 'Mon')? strtotime( "today" ) : strtotime( "previous monday" );

                if ($submissionWeek == $currentWeek) {
                    return false;
                } else {
                    if ($this->todaysDate >= strtotime('Thursday', $startReferenceDate)) {
                        return true;
                    }
                    return false;
                }
                break;
            case 'Current Period':
            case 'Every 3rd Period':
                $periodDate = $this->config->get('currentPeriod');
                $needsAttentionDate = strtotime(date('Y-m-d', $periodDate['start_date']) . '+3 week');


                if ($timePeriod == 'Every 3rd Period') {
                    $periodDate = $this->config->get('current3rdPeriod');
                    $needsAttentionDate = strtotime(date('Y-m-d', $periodDate['end_date']) . '-4 week');
                }
  
                if ($formSubmissionDate >= $periodDate['start_date'] && $formSubmissionDate <= $periodDate['end_date']) {
             
                    return false;
                } else {
                    if ($this->todaysDate <= $needsAttentionDate) {
                        return false;
                    } else {
                        return true;
                    }
                }
                break;
        }
    }

    public function getPeriod()
    {
        $year = date('Y');
        $vid = 'Periods';

        $ParentTerm = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $year, 'vid' => $vid]);

        if (empty($ParentTerm)) {
            $ParentTerm = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $year - 1, 'vid' => $vid]);
        }

        $ParentTerm = reset($ParentTerm);
        $childTerms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid, $parent = $ParentTerm->id(), $max_depth = 2, $load_entities = false);

        $lastPeriodOfYear = $this->config->get('yearLastPeriod');
        $date = date('Y-m-d');

        if (empty($lastPeriodOfYear) || strtotime($date . "00:00:00") > strtotime($lastPeriodOfYear['field_end_date'] . "23:59:59")) {
            $lastPeriod = end($childTerms);
            $lastPeriodData = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($lastPeriod->tid);
            $this->config->set('yearLastPeriod', $lastPeriodData->toArray())->save();
        }

        foreach ($childTerms as $term) {

            $periodTerm = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);

            if($periodTerm->field_start_date->value == null && $periodTerm->field_end_date->value == null){
                continue;
            }

            $periodStartDate = strtotime($periodTerm->field_start_date->value);
            $periodEndDate = strtotime($periodTerm->field_end_date->value);



            if ($this->todaysDate >= $periodStartDate && $this->todaysDate <= $periodEndDate) {
                $currentPeriod = [
                    'id' => $periodTerm->tid->value,
                    'name' => $periodTerm->name->value,
                    'start_date' => $periodStartDate,
                    'end_date' => $periodEndDate,
                ];

                $this->config->set('currentPeriod', $currentPeriod)->save();
            }

            $periods[$periodTerm->name->value] = [
                'id' => $periodTerm->tid->value,
                'name' => $periodTerm->name->value,
                'start_date' => $periodStartDate,
                'end_date' => $periodEndDate,
            ];
        }

        foreach ($periods as $period) {
            $periodName = explode("P", $period['name']);
            $end3rdPeriodNumber = end($periodName);
            if (($end3rdPeriodNumber % 3) == 0) {
                $start3rdPeriod = $periods['P' . strval(intval($end3rdPeriodNumber) - 2)];
                if ($this->todaysDate >= $start3rdPeriod['start_date'] && $this->todaysDate <= $period['end_date']) {
                    $this->config->set('current3rdPeriod', [
                        'start_date' => $start3rdPeriod['start_date'],
                        'end_date' => $period['end_date'],
                    ])->save();
                }
            }

        }
    }

}
