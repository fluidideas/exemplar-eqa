<?php

namespace Drupal\fluid_exemplar_reporting;

use Drupal\taxonomy\Entity\Term;

/**
 * Class HcApiService.
 */
class HcApiService
{

    protected $url = 'https://api-test.exemplarhc.com/exemplar_data/';
    protected $apiKey = '8c8041fb0fee653051c1f3a374d363b831a0a8c6';
    protected $options = [];
    /**
     * Constructs a new HcApiService object.
     */
    public function __construct()
    {
        $this->options = [
            'connect_timeout' => 30,
            'headers' => array(
                'Content-Type' => 'application/json',
                'Authorization' => ['Token ' . $this->apiKey],
            ),
            'verify' => true,
            'http_errors' => false,
        ];
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    public function getAllData($homeId = '')
    {
        $url = (!empty($homeId)) ? $this->url . $homeId : $this->url;
        try {
            $client = \Drupal::httpClient();
            $request = $client->request('GET', $url, $this->options);
        } catch (RequestException $e) {
            // Log the error.
            \Drupal::logger('my_module')->notice($e);
        }

        $responseStatus = $request->getStatusCode();
        $response = $request->getBody()->getContents();
        return json_decode($response);
    }

    public function getHomes()
    {
        try {
            $client = \Drupal::httpClient();
            $request = $client->request('GET', $this->url . 'homes', $this->options);
        } catch (RequestException $e) {
            // Log the error.
            \Drupal::logger('my_module')->notice($e);
        }

        $responseStatus = $request->getStatusCode();
        $response = $request->getBody()->getContents();
        return json_decode($response);
    }

    public function getUnits()
    {
        try {
            $client = \Drupal::httpClient();
            $request = $client->request('GET', $this->url . 'units', $this->options);
        } catch (RequestException $e) {
            // Log the error.
            \Drupal::logger('my_module')->notice($e);
        }

        $responseStatus = $request->getStatusCode();
        $response = $request->getBody()->getContents();
        return json_decode($response);
    }

    public function getRooms()
    {
        try {
            $client = \Drupal::httpClient();
            $request = $client->request('GET', $this->url . 'rooms', $this->options);
        } catch (RequestException $e) {
            // Log the error.
            \Drupal::logger('my_module')->notice($e);
        }

        $responseStatus = $request->getStatusCode();
        $response = $request->getBody()->getContents();
        return json_decode($response);
    }

    public function buildServiceUserTaxonomy()
    {
        // SELF::clearHomesTaxonomy();
        $vocabName = 'service_users';
        $homesData = SELF::getAllData();

        if (!empty($homesData)) {
            $this->cleanup($vocabName);
        }

        foreach ($homesData as $homes) {
            $homeName = $homes->name;

            if (!SELF::termExist($homeName, $vocabName)) {
                $new_term = Term::create([
                    'name' => $homeName,
                    'vid' => $vocabName,
                ]);
                $new_term->save();
            }
            //adds the home to the home list as well
            if (!SELF::termExist($homeName, 'homes')) {
                $new_term = Term::create([
                    'name' => $homeName,
                    'vid' => 'homes',
                ]);
                $new_term->save();
            }
            //sets the HomeParent id from the taxonomy just created
            $HomeParentID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $homeName, 'vid' => $vocabName]);
            $HomeParentID = reset($HomeParentID);

            foreach ($homes->units as $unit) {

                $unitName = $unit->name;

                $new_term = Term::create([
                    'name' => $unitName,
                    'vid' => $vocabName,
                    'parent' => ['target_id' => $HomeParentID->id()],
                ]);
                $new_term->save();

                //sets the Unity parent id from the taxonomy just created
                $UnitParentID = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $unitName, 'vid' => $vocabName, 'parent' => ['target_id' => $HomeParentID->id()],]);
                $UnitParentID = reset($UnitParentID);

                foreach ($unit->rooms as $room) {

                    $serviceUserForeName = $room->su_forename;
                    $serviceUserSurName = $room->su_surname;
                    $fullName = $serviceUserForeName . ' ' . $serviceUserSurName;

                    $new_term = Term::create([
                        'name' => $fullName,
                        'vid' => $vocabName,
                        'parent' => ['target_id' => $UnitParentID->id()],
                    ]);

                    $new_term->save();
                }
            }
        }
    }

    public function clearHomesTaxonomy()
    {
        //clears all the taxonomys in service_users
        $tids = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->getQuery()
            ->condition('vid', 'service_users')
            ->accessCheck(false)
            ->execute();

        $controller = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $entities = $controller->loadMultiple($tids);
        $controller->delete($entities);
    }

    public function termExist($name, $vocabName)
    {
        //checks to see if the taxonomy already exists
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
            ->loadByProperties(['name' => $name, 'vid' => $vocabName]);
        $term = reset($term);
        if ($term) {
            return true;
        }
        return false;
    }

    public function cleanup($vid)
    {
        $tids = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->getQuery()
            ->condition('vid', $vid)
            ->accessCheck(false)
            ->execute();

        if (empty($tids)) {
            return;
        }

        $term_storage = \Drupal::entityTypeManager()
            ->getStorage('taxonomy_term');
        $entities = $term_storage->loadMultiple($tids);

        $term_storage->delete($entities);
    }
}
