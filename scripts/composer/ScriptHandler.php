<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use Composer\Semver\Comparator;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class ScriptHandler {

  public static function renameTheme(Event $event) {
    $fs = new Filesystem();
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();
    $name = explode('/', getcwd());
    $name = $name[count($name)-1];

    $dirs = [
      'modules',
      'profiles',
      'themes',
    ];

    // Required for unit testing
    foreach ($dirs as $dir) {
      if (!$fs->exists($drupalRoot . '/'. $dir)) {
        $fs->mkdir($drupalRoot . '/'. $dir);
        $fs->touch($drupalRoot . '/'. $dir . '/.gitkeep');
      }
    }

    // Prepare the settings file for installation
    if (!$fs->exists($drupalRoot . '/sites/default/settings.php')) {
      $fs->copy(getcwd() . '/development.services.yml', $drupalRoot . '/sites/fluid.services.yml');
      $event->getIO()->write("Create a development.services.yml file");
      $fs->copy(getcwd() . '/settings.local.php', $drupalRoot . '/sites/settings.local.php');
      $event->getIO()->write("Create a settings.local.php file");
      $fs->copy(getcwd() . '/settings.php', $drupalRoot . '/sites/default/settings.php');
      $fs->chmod($drupalRoot . '/sites/default/settings.php', 0777);
      $event->getIO()->write("Create a sites/default/settings.php file with chmod 0777");
      $fs->mirror(getcwd() . '/fluid', $drupalRoot . '/profiles/fluid');
      $event->getIO()->write("Copying the Fluid installation profile");
    }

    // Create the files directory with chmod 0777
    if (!$fs->exists($drupalRoot . '/sites/default/files')) {
      $oldmask = umask(0);
      $fs->mkdir($drupalRoot . '/sites/default/files', 0777);
      umask($oldmask);
      $event->getIO()->write("Create a sites/default/files directory with chmod 0777");
    }
    if (!$fs->exists($drupalRoot . '/libraries')) {
      $oldmask = umask(0);
      $fs->mkdir($drupalRoot . '/libraries', 0755);
      umask($oldmask);
      $event->getIO()->write("Creates libraries folder");
      $fs->mirror(getcwd() . '/vendor/enyo', $drupalRoot . '/libraries');
      $fs->mirror(getcwd() . '/vendor/desandro', $drupalRoot . '/libraries');
    }

//    $name = $event->getArguments()[0];
    $fs->rename($drupalRoot . '/themes/kitt/kitt_sub_theme', $drupalRoot . '/themes/'.$name);
    $fs->rename($drupalRoot . '/themes/'.$name.'/kitt_sub_theme.breakpoints.yml', $drupalRoot . '/themes/'.$name.'/'.$name.'.breakpoints.yml');
    $fs->rename($drupalRoot . '/themes/'.$name.'/kitt_sub_theme.info.yml', $drupalRoot . '/themes/'.$name.'/'.$name.'.info.yml');
    $fs->rename($drupalRoot . '/themes/'.$name.'/kitt_sub_theme.libraries.yml', $drupalRoot . '/themes/'.$name.'/'.$name.'.libraries.yml');
    $fs->rename($drupalRoot . '/themes/'.$name.'/kitt_sub_theme.theme', $drupalRoot . '/themes/'.$name.'/'.$name.'.theme');
    $fc = file_get_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.theme');
    $fc = str_replace('kitt_sub_theme', $name, $fc);
    file_put_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.theme', $fc);
    $fc = file_get_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.theme');
    $fc = str_replace('kitt_sub_theme', $name, $fc);
    file_put_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.theme', $fc);
    $fc = file_get_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.info.yml');
    $fc = str_replace('kitt_sub_theme', $name, $fc);
    $fc = str_replace('Kitt', ucfirst($name), $fc);
    file_put_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.info.yml', $fc);
    $fc = file_get_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.breakpoints.yml');
    $fc = str_replace('kitt_sub_theme', $name, $fc);
    file_put_contents($drupalRoot . '/themes/'.$name.'/'.$name.'.breakpoints.yml', $fc);
  }


  public static function checkComposerVersion(Event $event) {
    $composer = $event->getComposer();
    $io = $event->getIO();

    $version = $composer::VERSION;

    // The dev-channel of composer uses the git revision as version number,
    // try to the branch alias instead.
    if (preg_match('/^[0-9a-f]{40}$/i', $version)) {
      $version = $composer::BRANCH_ALIAS_VERSION;
    }

    // If Composer is installed through git we have no easy way to determine if
    // it is new enough, just display a warning.
    if ($version === '@package_version@' || $version === '@package_branch_alias_version@') {
      $io->writeError('<warning>You are running a development version of Composer. If you experience problems, please update Composer to the latest stable version.</warning>');
    }
    elseif (Comparator::lessThan($version, '1.0.0')) {
      $io->writeError('<error>Drupal-project requires Composer version 1.0.0 or higher. Please update your Composer before continuing</error>.');
      exit(1);
    }
  }

}
